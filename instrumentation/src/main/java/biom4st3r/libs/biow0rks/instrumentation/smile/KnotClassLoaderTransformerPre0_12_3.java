// package biom4st3r.libs.biow0rks.instrumentation.smile;

// import net.fabricmc.loader.impl.FabricLoaderImpl;

// import biom4st3r.libs.biow0rks.asm.OpcodeMethodVisitor;
// import org.objectweb.asm.ClassVisitor;
// import org.objectweb.asm.MethodVisitor;
// import org.objectweb.asm.Type;

// public class KnotClassLoaderTransformerPre0_12_3 extends ClassVisitor {
//     public KnotClassLoaderTransformerPre0_12_3(int api, ClassVisitor classVisitor) {
//         super(api, classVisitor);
//     }

//     public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
//         MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
//         if(name.equals("loadClass")) {
//             return new MethodVisitor(FabricLoaderImpl.ASM_VERSION, mv){
//                 public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
//                     if(name.equals("getPostMixinClassByteArray")) {
//                         OpcodeMethodVisitor ops = new OpcodeMethodVisitor(this);
//                         ops
//                             .SWAP() // swap delegate and name
//                             .POP() // pop delegate
//                             .INVOKE_STATIC(Type.getInternalName(Smile.class), "postMixinReTransform", Type.getMethodDescriptor(Type.getType(byte[].class), Type.getType(String.class)));
//                         return;
//                         // ops.ALOAD(1).INVOKE_STATIC(Type.getInternalName(Smile.class), "postMixinReTransform", Type.getMethodDescriptor(Type.getType(byte[].class), Type.getType(byte[].class),Type.getType(String.class)));
//                     }
//                     super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
//                 };
//             };
//         }
//         return mv;
//     }


// }