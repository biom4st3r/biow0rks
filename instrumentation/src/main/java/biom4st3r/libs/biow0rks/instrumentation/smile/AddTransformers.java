// package biom4st3r.libs.biow0rks.instrumentation.smile;

// import java.io.File;
// import java.io.FileInputStream;
// import java.io.InputStream;
// import java.util.List;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

// import biom4st3r.libs.biow0rks.NoEx;
// import com.google.common.io.ByteStreams;

// public interface AddTransformers {
//     List<byte[]> getTransformers();

//     public static List<byte[]> fromFiles(File... files) {
//         return Stream.of(files).map(f -> NoEx.run(()->new FileInputStream(f))).map(s->NoEx.run(()->ByteStreams.toByteArray(s))).collect(Collectors.toList());
//     }

//     public static List<byte[]> fromInputStream(InputStream... stream) {
//         return Stream.of(stream).map(s->NoEx.run(()->ByteStreams.toByteArray(s))).collect(Collectors.toList());
//     }
// }
