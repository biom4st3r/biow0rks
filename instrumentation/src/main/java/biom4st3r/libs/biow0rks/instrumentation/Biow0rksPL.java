package biom4st3r.libs.biow0rks.instrumentation;

import java.io.File;
import java.lang.instrument.Instrumentation;
import java.lang.management.ManagementFactory;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.jetbrains.annotations.Nullable;

import biom4st3r.libs.biow0rks.BioLogger;
import biom4st3r.libs.biow0rks.NoEx;
import biom4st3r.libs.biow0rks.annotations.PlainTxtTarget;
import biom4st3r.libs.biow0rks.instrumentation.smile.Smile;
import biom4st3r.libs.biow0rks.reflection.FieldHandler;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;
import net.fabricmc.loader.impl.ModContainerImpl;

@PlainTxtTarget
public class Biow0rksPL implements PreLaunchEntrypoint {
    public static final boolean MASS_ASM_QUESTION_MARK = true;
    // private static final class TransformerLoader extends ClassLoader {
    //     public Class<?> loadTransformer(byte[] b) {
    //         return this.defineClass(null, b, 0, b.length);
    //     }
    // }

    public static final BioLogger logger = new BioLogger("Biow0rksPL");
    public static final String SMILE = "biom4st3r.libs.biow0rks.instrumentation.smile.Smile";
    public static final String KNOT_CLASSLOADER = "net.fabricmc.loader.impl.launch.knot.KnotClassLoader";
    public static final String KNOT_CLASS_DELEGATE = "net.fabricmc.loader.impl.launch.knot.KnotClassDelegate";

    public static Class<?> getAppSmile() {
        try {
            ClassLoader cl = getAppClassLoader();
            return cl.loadClass(SMILE);
        } catch (ClassNotFoundException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static Class<?> getKnotSmile() {
        try {
            ClassLoader cl = getKnotClassLoader();
            return cl.loadClass(SMILE);
        } catch (ClassNotFoundException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static ClassLoader getAppClassLoader() {
        try {
            return Class.forName(KNOT_CLASSLOADER).getClassLoader();
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException();
        }
    }

    public static ClassLoader getKnotClassLoader() {
        if (Smile.knot == getAppClassLoader()) throw new IllegalStateException();
        return Smile.knot;
    }

    private static File getBiow0rksJar() {
        if (FabricLoader.getInstance().isDevelopmentEnvironment()) {
            Path path = new File(FabricLoader.getInstance().getConfigDir().toFile(), "../mods/").toPath();
            for(File file : path.toFile().listFiles()) {
                if(file.getName().contains("biow0rks-instrumentation")) {
                    return file;
                }
            }
            return null;
        }

        ModContainerImpl impl = (ModContainerImpl) FabricLoader.getInstance().getModContainer("biow0rks_instrumentation").get();
        boolean isDevelopment = impl.getOrigin().toString().endsWith("\\bin\\main");
        if(!isDevelopment) return impl.getRootPath().toFile();

        String s = FabricLoader.getInstance().getConfigDir().toFile().getParent() + "\\mods\\";
        File[] file = new File(s).listFiles();
        File jar = Stream.of(file).filter(f->f.getName().contains("biow0rks-instrumentation")).findFirst().get();
        return jar;
    }

    @SuppressWarnings({"unchecked","deprecation"})
    private static void addSoftwareLibrary(File file) {
        Class<?> clazz = NoEx.run(()->Class.forName("jdk.internal.loader.BuiltinClassLoader"));
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        Object ucp = FieldHandler.get(clazz, "ucp", -1, false).unsafeGetField(cl, Object.class);
        ArrayDeque<URL> unopenedurls = (ArrayDeque<URL>) FieldHandler.get(ucp.getClass(), "unopenedUrls", -1, false).unsafeGetField(ucp, Object.class);
        unopenedurls.add(NoEx.run(()->file.toURI().toURL()));
    }

    static final Set<String> NAMES = Sets.newHashSet("net.fabricmc.loader.impl.launch.knot.KnotClassLoader","net.fabricmc.loader.launch.knot.KnotClassLoader");

    @Override
    @SuppressWarnings({"deprecation"})
    public void onPreLaunch() {
        if (!MASS_ASM_QUESTION_MARK) return;

        String s = this.getClass().getClassLoader().getClass().getName();
        if(!NAMES.contains(s)) {
            logger.error("UNKNOWN CLASSLOADER %s BIOW0RKS ABORTING INSTRUMENTATION", s);
            return;
        }

        Class<?> clazz = NoEx.run(()->Class.forName("sun.tools.attach.HotSpotVirtualMachine"));
        FieldHandler<Boolean> ALLOW_ATTACH_SELF = FieldHandler.get(clazz, "ALLOW_ATTACH_SELF", -1, false);
        // shhhhh....just let it happen
        // You can't just add System.getProperties().setProperty("jdk.attach.allowAttachSelf", "true");
        // It checks the original Properties. so......
        ALLOW_ATTACH_SELF.setFinalField(null, true);

        // Find biow0rks jar
        File jar = getBiow0rksJar();
        // Add biow0rks jar to AppClassloader URL list for Instrumentation
        addSoftwareLibrary(jar);
        // Load an instance of Smile to the App classloader
        // We'll use it later
        prepareSmileForAgent();
        // INIT DONE

        NoEx.run(()-> { // get the Instrumentation
            // Find VM
            String vmname = ManagementFactory.getRuntimeMXBean().getName();
            String pid = vmname.substring(0, vmname.indexOf('@'));
            com.sun.tools.attach.VirtualMachine vm = com.sun.tools.attach.VirtualMachine.attach(pid);
            // Load agent from jar
            vm.loadAgent(jar.getAbsolutePath());
            vm.detach();
        });

        
        // ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        // ClassReader cr = AsmUtil.getReader(KNOT_CLASS_DELEGATE);

        // ClassVisitor cv = new KnotClassDelegateTransformer(FabricLoaderImpl.ASM_VERSION, cw);
        // cr.accept(cv, ClassReader.SKIP_FRAMES);

        // NoEx.run(() -> Smile.hidyhole.redefineClasses(new ClassDefinition(Class.forName(KNOT_CLASS_DELEGATE), cw.toByteArray())));
    }

    private static void storeInField(String name, Class<?> target, @Nullable Object host, Object value) {
        FieldHandler.get(target, name, 0, false).set(host, value);
    }

    private void prepareSmileForAgent() {
        ClassLoader cl = Smile.knot = this.getClass().getClassLoader();
        // Store knotCL for later
        storeInField("knot", getKnotSmile(), null, cl);
        storeInField("knot", getAppSmile(), null, cl);
    }

    public static Iterable<Path> findJars() {
        List<Path> mods = Lists.newArrayListWithExpectedSize(FabricLoader.getInstance().getAllMods().size()+5);

        Optional<ModContainer> gameJar = FabricLoader.getInstance().getModContainer("minecraft");
        mods.add(NoEx.run(()->Path.of(((ModContainerImpl)gameJar.get()).getOriginUrl().toURI())));
        
        for (net.fabricmc.loader.api.ModContainer container : FabricLoader.getInstance().getAllMods()) {
            ModContainerImpl impl = (ModContainerImpl) container;
            
            Path path = NoEx.run(()->Path.of(impl.getOriginUrl().toURI()));
            if (!path.toString().endsWith(".jar")) continue;
            if (impl.getMetadata().getType().equals("builtin")) continue;

            mods.add(path);
        }
        return mods;
    }

    private static void initAsmr() {
        // AsmrPlatform platform = new AsmrPlatform() {
        //     final Object knotClassDelegate = FieldHandler.get(getKnotClassLoader().getClass(), "delegate", -1, true).get(getKnotClassLoader());
        //     final MethodRef<byte[]> method$getPostMixinClassByteArray = MethodRef.getMethod(knotClassDelegate.getClass(), "getPostMixinClassByteArray", String.class, boolean.class);
        //     final BiFunction<String, Boolean, byte[]> getPostMixinClassBytes = (name, allowFromParent) -> method$getPostMixinClassByteArray.invoke(knotClassDelegate, name, allowFromParent);
            
        //     @Override
        //     public byte[] getClassBytecode(String name) throws ClassNotFoundException {
        //         return getPostMixinClassBytes.apply(name, false);
        //     }
        // };
        // TransformerLoader transformerLoader = new TransformerLoader();

        // Function<byte[], AsmrTransformer> loader = (b)-> {
        //     return (AsmrTransformer) NoEx.runFunc(transformerLoader.loadTransformer(b).getConstructors()[0]::newInstance);
        // };

        // AsmrProcessor processor = new AsmrProcessor(platform, loader);
        // findJars().forEach(p -> processor.addJar(p, null));

        // storeInField("platform", getAppSmile(), null, platform);
        // storeInField("processor", getAppSmile(), null, processor);
        // List<AddTransformers> ts = FabricLoader.getInstance().getEntrypoints("biow0rks:supplyTransformers", AddTransformers.class);
        // ts.forEach(at -> at.getTransformers().forEach(b -> processor.addTransformer(b)));
        // processor.process();
    }

    public static void premain(String arg, Instrumentation instrumentation) {
        agentmain(arg, instrumentation);
    }

    public static void agentmain(String arg, Instrumentation instrumentation) {
        logger.log("I solemnly swear that I am up to no good.");
        // logger.log(getKnotClassLoader().getClass().getName() + " | " + getAppClassLoader().getClass().getName());
        // Instrumentation was loaded on the App Classload not Knot.
        // So we can just store instrumentation in Smile. Reflection probably isn't needed
        // But this is more descriptive
        storeInField("hidyhole", getAppSmile(),  null, instrumentation);
        storeInField("hidyhole", getKnotSmile(), null, instrumentation);
        initAsmr();
    }
}
