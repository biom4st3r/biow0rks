package biom4st3r.libs.biow0rks.instrumentation.smile;

import java.lang.instrument.Instrumentation;

import org.objectweb.asm.Opcodes;

import biom4st3r.libs.biow0rks.BioLogger;
import biom4st3r.libs.biow0rks.annotations.PlainTxtTarget;

/**
 * You most likely can not access these fields.
 */
@PlainTxtTarget
public class Smile implements Opcodes {

    @PlainTxtTarget
    public static Instrumentation hidyhole;

    @PlainTxtTarget
    public static ClassLoader knot;

    public static final BioLogger logger = new BioLogger("Smile");

    // @AppField
    // @PlainTxtTarget
    // public static AsmrPlatform platform;

    // @AppField
    // @PlainTxtTarget
    // public static AsmrProcessor processor;

    // /**
    //  * 
    //  * @param classBuffer
    //  * @param name dot format
    //  * @return
    //  */
    // @PlainTxtTarget
    // public static byte[] postMixinReTransform(String name) {
    //     AsmrClassWriter wr = new AsmrClassWriter(processor);
    //     AsmrClassNode clazz = processor.findClassImmediately(name);
    //     if(clazz != null) {
    //         clazz.accept(wr);
    //         return wr.toByteArray();
    //     } else {
    //         return NoEx.run(() -> platform.getClassBytecode(name));
    //     }
    // }
}
