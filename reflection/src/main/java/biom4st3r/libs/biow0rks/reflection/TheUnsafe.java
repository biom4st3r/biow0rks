package biom4st3r.libs.biow0rks.reflection;

import java.lang.reflect.Field;
import java.util.function.Supplier;
import java.util.stream.Stream;

public final class TheUnsafe {
    public static final sun.misc.Unsafe UNSAFE;
    static {
        final Field unsafeField = Stream.of(sun.misc.Unsafe.class.getDeclaredFields()).filter(f->f.getType() == sun.misc.Unsafe.class).peek(f->f.setAccessible(true)).findAny().get();
        final Supplier<sun.misc.Unsafe> unsafeSupplier = ()-> {
            try {
                return (sun.misc.Unsafe) unsafeField.get(null);
            } catch(Throwable t) {
                throw new RuntimeException(t);
            }
        };
        UNSAFE = unsafeSupplier.get();
    }
}
