package biom4st3r.libs.biow0rks.reflection;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Method;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface MethodRef<R,T> {

    R invoke(Object host, Object... obj);

    T getMethod() throws Throwable;

    static interface ReflectMethodRef<R> extends MethodRef<R,Method> {

        @Override
        @SuppressWarnings("unchecked")
        default R invoke(Object host, Object... obj) {
            try {
                return (R) getMethod().invoke(host, obj);
            } catch (Throwable t) {
                t.printStackTrace();
                return null;
            }
        }
    }

    static interface HandleMethodRef<R> extends MethodRef<R,MethodHandle> {
        @Override
        default R invoke(Object host, Object... obj) {
            try {
                return (R) getMethod().invoke(host, obj);
            } catch (Throwable t) {
                t.printStackTrace();
                return null;
            }
        }
    }

    static <R> MethodRef<R,Method> getMethod(Class<?> target, String name, boolean setAccessible, Class<?>... args) {
        try {
            Method method = target.getDeclaredMethod(name, args);
            if(setAccessible) method.setAccessible(true);
            return (ReflectMethodRef<R>)() -> method;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    static <R> MethodRef<R,Method> getMethod(Class<?> target, boolean setAccessible, Predicate<Method> pred) {
        Method method = Stream.of(target.getDeclaredMethods()).filter(pred).findFirst().get();
        try {
            if(setAccessible) method.setAccessible(true);
            return (ReflectMethodRef<R>)() -> method;
        } catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }

    static <R> MethodRef<R,MethodHandle> getMethodUnrestricted(Class<?> target, String name, Class<?>... args) {
        try {
            Method method = target.getDeclaredMethod(name, args);
            MethodHandle mh = GodMode.GOD.unreflect(method);
            return (HandleMethodRef<R>)() -> mh;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    static <R> MethodRef<R,MethodHandle> getMethodUnrestricted(Class<?> target, boolean setAccessible, Predicate<Method> pred) {
        Method method = Stream.of(target.getDeclaredMethods()).filter(pred).findFirst().get();
        try {
            MethodHandle mh = GodMode.GOD.unreflect(method);
            return (HandleMethodRef<R>)() -> mh;
        } catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
