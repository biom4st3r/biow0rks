package biom4st3r.libs.biow0rks.reflection;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.function.Function;

import biom4st3r.libs.biow0rks.NoEx;

public final class GodMode {
    public static final Lookup GOD, TRUSTED;
    public static final Function<Class<?>,Lookup> godCreator;
    static {
        Field IMPL_LOOKUP = NoEx.run(()->Lookup.class.getDeclaredField("IMPL_LOOKUP"));
        long offset = TheUnsafe.UNSAFE.staticFieldOffset(IMPL_LOOKUP);
        Object BASE = TheUnsafe.UNSAFE.staticFieldBase(IMPL_LOOKUP);
        TRUSTED = (Lookup) TheUnsafe.UNSAFE.getObject(BASE, offset);
        Constructor<?> newLookup = NoEx.run(()->Lookup.class.getDeclaredConstructor(Class.class,Class.class,int.class));
        MethodHandle lookupCtor = NoEx.run(()->TRUSTED.unreflectConstructor(newLookup));
        godCreator = (clazz) -> (Lookup) NoEx.run(()->lookupCtor.invokeWithArguments(clazz, null, 127 | -1));
        GOD = godCreator.apply(Lookup.class);
    }
}
