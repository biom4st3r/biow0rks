package biom4st3r.libs.biow0rks.reflection;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface FieldHandler<T> {

    T get(Object o);
    void set(Object o, T t);

    static class _FieldHandler<T> implements FieldHandler<T> {
        final Field field;

        public _FieldHandler(Field f) {
            this.field = f;
        }
        @Override
        @SuppressWarnings({"unchecked"})
        public T get(Object o) {
            try {
                return (T) field.get(o);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void set(Object o, T t) {
            try {
                field.set(o, t);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void setFinalField(Object o, T t) {
            FieldHandler.unsafeSetField(this, o, t);
        }

        @Override
        public Object unsafeGetField(Object o, Class<?> type) {
            return FieldHandler.unsafeGetField(this, o, type);
        }
    }

    @Deprecated
    default void setFinalField(Object o, T t) {
    }

    @Deprecated
    default Object unsafeGetField(Object o, Class<?> type) {
        return null;
    }

    public static void unsafeSetField(FieldHandler<?> fieldhand, Object host, Object value) {
        Field field = ((_FieldHandler<?>)fieldhand).field;
        sun.misc.Unsafe unsafe = TheUnsafe.UNSAFE;
        long offset = host == null ? unsafe.staticFieldOffset(field) : unsafe.objectFieldOffset(field);
        if(host == null) host = field.getDeclaringClass();

        if(value instanceof Boolean b) unsafe.putBoolean(host, offset, b);
        else if(value instanceof Byte b) unsafe.putByte(host, offset, b);
        else if(value instanceof Short s) unsafe.putShort(host, offset, s);
        else if(value instanceof Integer i) unsafe.putInt(host, offset, i);
        else if(value instanceof Long l) unsafe.putLong(host, offset, l);
        else if(value instanceof Float f) unsafe.putFloat(host, offset, f);
        else if(value instanceof Double b) unsafe.putDouble(host, offset, b);
        else if(value instanceof Character c) unsafe.putChar(host, offset, c);
        else unsafe.putObject(host, offset, value);
        return;

    }

    public static Object unsafeGetField(FieldHandler<?> fieldhand, Object host, Class<?> type) {
        Field field = ((_FieldHandler<?>)fieldhand).field;
        sun.misc.Unsafe unsafe = TheUnsafe.UNSAFE;
        long offset = host == null ? unsafe.staticFieldOffset(field) : unsafe.objectFieldOffset(field);
        if (host == null) host = field.getDeclaringClass(); //unsafe.staticFieldBase(f)
        
        if(type == Boolean.class) return unsafe.getBoolean(host, offset);
        else if(type == Byte.class) return unsafe.getByte(host, offset);
        else if(type == Short.class) return unsafe.getShort(host, offset);
        else if(type == Integer.class) return unsafe.getInt(host, offset);
        else if(type == Long.class) return unsafe.getLong(host, offset);
        else if(type == Float.class) return unsafe.getFloat(host, offset);
        else if(type == Double.class) return unsafe.getDouble(host, offset);
        else if(type == Character.class) return unsafe.getChar(host, offset);
        else return unsafe.getObject(host, offset);
    }

    static <T> FieldHandler<T> get(Class<?> target, String name, int index, boolean setAccessible) {
        Field f = null;
        if(name != null) {
            try {
                f = target.getDeclaredField(name);
            } catch (NoSuchFieldException | SecurityException e) {
                throw new RuntimeException(e);
            }
        } else {
            f = target.getDeclaredFields()[index];
        }

        final Field field = f;
        if(setAccessible) f.setAccessible(true);

        return new _FieldHandler<>(field);
    }

    static <T> FieldHandler<T> get(Class<?> target, Predicate<Field> isField) {
        Optional<Field> optional = Stream.of(target.getDeclaredFields()).filter(isField).findFirst();
        if (optional.isEmpty()) throw new IllegalStateException("Field not found");
        final Field field = optional.get();
        field.setAccessible(true);

        return new _FieldHandler<>(field);
    }
}

