package biom4st3r.libs.biow0rks.reflection;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Constructor;
import java.util.function.Predicate;
import java.util.stream.Stream;

import biom4st3r.libs.biow0rks.NoEx;

public interface CtorRef<R,T> {
    R newInstance(Object... objs);
    T _ctor() throws Throwable;

    static interface ReflectedCtorRef<R> extends CtorRef<R,Constructor<?>> {
        @Override
        @SuppressWarnings("unchecked")
        default R newInstance(Object... objs) {
            try {
                return (R) _ctor().newInstance(objs);
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        }
    }
    static interface MHCtorRef<R> extends CtorRef<R,MethodHandle> {
        @Override
        @SuppressWarnings("unchecked")
        default R newInstance(Object... objs) {
            try {
                return (R) _ctor().invokeWithArguments(objs);
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        }
    }

    static <R> CtorRef<R,Constructor<?>> getCtor(Class<?> target, boolean setAccessible, Class<?>... args) {
        Constructor<?> ctor = NoEx.run(()->target.getDeclaredConstructor(args));
        if(setAccessible) ctor.setAccessible(true);
        return (ReflectedCtorRef<R>)()->ctor;
    }

    static <R> CtorRef<R,Constructor<?>> getCtor(Class<?> target, boolean setAccessible, Predicate<Constructor<?>> pred) {
        Constructor<?> ctor = Stream.of(target.getDeclaredConstructors()).filter(pred).peek(c -> {if(setAccessible) c.setAccessible(true);}).findFirst().get();
        return (ReflectedCtorRef<R>)()->ctor;
    }

    static <R> CtorRef<R,MethodHandle> getCtorUnrestricted(Class<?> target, Class<?>... args) {
        Constructor<?> ctor = NoEx.run(()->target.getDeclaredConstructor(args));
        MethodHandle handle = NoEx.run(()->GodMode.GOD.unreflectConstructor(ctor));
        return (MHCtorRef<R>)()->handle;
    }

}
