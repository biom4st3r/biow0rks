package biom4st3r.libs.biow0rks.asm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

public class StackTrackingMethodNode extends MethodNode implements Opcodes {

    public StackTrackingMethodNode(int access, String name, String descriptor, String signature,
            String[] exceptions) {
        super(ASM9, access, name, descriptor, signature, exceptions);
        // this.instructions = new InsnList() {
        //     @Override
        //     public void add(AbstractInsnNode insnNode) {
        //         onInstructionAdded(insnNode);
        //         super.add(insnNode);
        //     }
        // };
    }

    private static <T> int findEmptySlot(T[] a) {
        for(int i = 0; i < a.length; i++) {
            if(a[i] == null) return i;
        }
        return a.length;
    }

    @SuppressWarnings("unused")
    private static <T> void fillArray(Supplier<T> supplier, T[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = supplier.get();
        }
    }

    @SuppressWarnings("unused")
    private static record InsnBundle(AbstractInsnNode node, List<AbstractInsnNode> dependents){
        public void remove(InsnList list) {
            list.remove(node);
            for(AbstractInsnNode x : dependents) {
                list.remove(x);
            }
        }
    }

    public static InsnBundle of(AbstractInsnNode node) {
        return new InsnBundle(node, Lists.newArrayList());
    }


    List<InsnBundle[]> stackAtInsn;
    int i = 0;
    public void onInstructionAdded(AbstractInsnNode insnNode) {
        InsnBundle[] a = i == 0 ? new InsnBundle[this.maxStack] : Arrays.copyOf(stackAtInsn.get(i-1), this.maxStack);
        int op = insnNode.getOpcode();
        int slot = findEmptySlot(a);
        enactInsn(insnNode, a, op, slot);
        stackAtInsn.add(a);

        i++;
    }

    @Override
    public void visitEnd() {
        this.stackAtInsn = new ArrayList<>(this.instructions.size());
        for(AbstractInsnNode insn : this.instructions) {
            onInstructionAdded(insn);
        }
        super.visitEnd();
    }
    

    private void enactInsn(AbstractInsnNode insnNode, InsnBundle[] a, int op, int slot) {
        try {
            if(op == DCONST_0 || op == DCONST_1 || op == LCONST_0 || op == LCONST_1 || op == LLOAD || op == DLOAD) { // PUSH
                a[slot] = of(insnNode);
                a[slot+1] = of(insnNode);
            } else if(op > 0 && op <= ALOAD && op != LDC) { // PUSH
                a[slot] = of(insnNode);
            } else if(op >= IALOAD && op <= SALOAD) { // POP AND PUSH
                a[slot-1] = null; // Array ref
                a[slot-2] = of(insnNode); // index
            } else if(op == LDC) { // PUSH
                LdcInsnNode ldc = (LdcInsnNode) insnNode;
                if(ldc.cst instanceof Double || ldc.cst instanceof Long) {
                    a[slot] = of(insnNode);
                    a[slot+1] = of(insnNode);
                } else {
                    a[slot] = of(insnNode);
                }
            } else if(op == DSTORE || op == LSTORE) { // POP
                a[slot] = null;
                a[slot-1] = null;
            } else if(op >= ISTORE && op <= ASTORE) { // POP
                a[slot] = null;
            } else if(op == DASTORE || op == LASTORE) { // POP
                a[slot] = null; // double ref
                a[slot-1] = null; // double ref
                a[slot-2] = null; // array ref
                a[slot-3] = null; // index
            } else if(op >= IASTORE && op <= SASTORE) { // POP
                a[slot] = null; // double ref
                a[slot-1] = null; // array ref
                a[slot-2] = null; // index
            } else if(op == POP) { // POP
                a[slot] = null;
            } else if(op == POP2) { // POP
                a[slot] = null;
                a[slot-1] = null;
            } else if(op == DUP) { // PUSH  // DEPENDANT
                a[slot] = of(insnNode);
                a[slot].dependents.add(a[slot-1].node);
            } else if(op == DUP_X1) {
                
            } else if(op == DUP_X2) {
                
            } else if(op == DUP2) {
                
            } else if(op == DUP2_X1) {
                
            } else if(op == DUP2_X1) {
                
            } else if(op == SWAP) { // DEPENDANT
                InsnBundle t = a[slot-1];
                a[slot-1] = a[slot-2];
                a[slot-2] = t;
                a[slot-1].dependents.add(insnNode);
                a[slot-2].dependents.add(insnNode);
            } else if(op == LADD || op == DADD || op == LSUB || op == DSUB || op == LMUL || op == DMUL || op == LDIV || op == DDIV || op == LREM || op == DREM) { // POP and PUSH
                a[slot-1] = null;
                a[slot-2] = null;
                a[slot-3] = of(insnNode);
                a[slot-4] = of(insnNode);
            } else if(op >= IADD && op <= DREM) { // POP and PUSH
                a[slot-1] = null;
                a[slot-2] = of(insnNode);
            } else if(op >= INEG && op <= DNEG) { // DEPENDANT
                a[slot].dependents.add(insnNode);
                // NO CHANGE
            } else if(op == LSHL || op == LSHR || op == LUSHR || op == LAND || op == LOR || op == LXOR) { // POP and PUSH
                a[slot-1] = null;
                a[slot-2] = null;
                a[slot-3] = of(insnNode);
                a[slot-4] = of(insnNode);
            } else if(op >= ISHL && op <= LXOR) { // POP and PUSH
                a[slot-1] = null;
                a[slot-2] = of(insnNode);
            } else if(op == IINC) { 
                // NO CHANGE
            } else if(op == I2L || op == I2D || op == F2L || op == F2D) {
                a[slot-1] = of(insnNode);
                a[slot] = of(insnNode);
            } else if(op == L2I || op == D2I || op == L2F || op == D2F) {
                a[slot-1] = null;
                a[slot-2] = of(insnNode);
            } else if(op == D2L || op == L2D) { // DEPENDANT
                a[slot-1].dependents.add(insnNode);
            } else if(op >= I2L && op <= I2S) {
                a[slot-1] = of(insnNode);
            } else if(op == LCMP || op == DCMPL || op == DCMPG) {
                a[slot-1] = null;
                a[slot-2] = of(insnNode);
            } else if(op >= LCMP && op <=DCMPG) {
                a[slot-1] = of(insnNode);
            } else if(op >=IFEQ && op <= IFLE) {
                a[slot-1] = null;
            } else if(op >=IF_ICMPEQ && op <= IF_ACMPNE) {
                a[slot-1] = null;
                a[slot-2] = null;
            } else if(op == GOTO || op == RET) {
                // NO CHANGE
            } else if(op == JSR) {
                // TODO
            } else if(op == TABLESWITCH || op == LOOKUPSWITCH) {
                a[slot-1] = null;
            } else if(op >= IRETURN && op <= RETURN) {
                // NO CHANGE
            } else if(op == GETSTATIC) {
                Type type = Type.getType(((FieldInsnNode)insnNode).desc);
                if(type == Type.getType(long.class) || type== Type.getType(double.class)) {
                    a[slot] = of(insnNode);
                    a[slot+1] = of(insnNode);
                } else {
                    a[slot] = of(insnNode);
                }
            } else if(op == PUTSTATIC) {
                Type type = Type.getType(((FieldInsnNode)insnNode).desc);
                if(type == Type.getType(long.class) || type== Type.getType(double.class)) {
                    a[slot-1] = null;
                    a[slot-2] = null;
                } else {
                    a[slot-1] = null;
                }
            } else if(op == GETFIELD) { // Depend on aload
                Type type = Type.getType(((FieldInsnNode)insnNode).desc);
                if(type == Type.getType(long.class) || type== Type.getType(double.class)) {
                    a[slot-1] = of(insnNode);
                    a[slot] = of(insnNode);
                } else {
                    a[slot-1] = of(insnNode);
                }
            } else if(op == PUTFIELD) { // Depend on aload
                Type type = Type.getType(((FieldInsnNode)insnNode).desc);
                if(type == Type.getType(long.class) || type== Type.getType(double.class)) {
                    a[slot-1] = null;
                    a[slot-2] = null;
                    a[slot-3] = null;
                } else {
                    a[slot-1] = null;
                    a[slot-2] = null;
                }
            } else if(op >= INVOKEVIRTUAL && op <= INVOKEINTERFACE) {
                MethodInsnNode mn = (MethodInsnNode) insnNode;
                Type rt = Type.getReturnType(mn.desc);
                Type[] types = Type.getArgumentTypes(mn.desc);
                InsnBundle d = of(insnNode);
                for(int i = 0; i < types.length; i++) {
                    d.dependents.add(a[slot-1].node);
                    d.dependents.addAll(a[slot-1].dependents);
                    a[--slot] = null;
                }
                if(op == INVOKESTATIC) slot++;
                if(!rt.equals(Type.getType(void.class))) {
                    a[slot-1] = d;
                } else {
                    a[slot-1] = null;
                }
            }
        }
        catch(Exception e) {
            System.out.println(insnNode);
            System.out.println(slot);
            System.out.println(op);
            throw new RuntimeException(e);
        }
    }

}
