package biom4st3r.libs.biow0rks.asm;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.google.common.collect.Maps;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class AsmPrimitive implements Opcodes {

    public static enum Stack {
        LOAD,
        STORE
    }

    public static interface TriConsumer<A,B,C> {
        void accept(A a, B b, C c);
    }

    private static Map<Class<?>,AsmPrimitive> primitives = Maps.newHashMap();
    public final Class<?> clazz;
    private final Consumer<MethodVisitor> doReturn;
    private final BiConsumer<Stack,MethodVisitor> touchArray;
    private final TriConsumer<Stack,MethodVisitor,Integer> touchVar;
    private final Consumer<MethodVisitor> loadDefaultValue;
    private final int arrayType;

    public void _return(MethodVisitor mv) {
        this.doReturn.accept(mv);
    }

    public void touchArray(Stack stack, MethodVisitor mv) {
        this.touchArray.accept(stack, mv);
    }

    public void touchVar(Stack stack, MethodVisitor mv, int i) {
        this.touchVar.accept(stack,mv,i);
    }

    public int arrayType() {
        return this.arrayType;
    }

    public void loadDefaultValue(MethodVisitor mv) {
        this.loadDefaultValue.accept(mv);
    }

    public void newArray(int size, MethodVisitor mv, String nullOrClassNameOrGarbage) {
        // Load size
        if(size >= -1 && size <=5) {
            switch (size) {
                case -1:
                    mv.visitInsn(ICONST_M1);
                    break;
                case 0:
                    mv.visitInsn(ICONST_0);
                    break;
                case 1:
                    mv.visitInsn(ICONST_1);
                    break;
                case 2:
                    mv.visitInsn(ICONST_2);
                    break;
                case 3:                   
                    mv.visitInsn(ICONST_3);
                    break;
                case 4:
                    mv.visitInsn(ICONST_4);
                    break;
                case 5:
                    mv.visitInsn(ICONST_5);
                    break;
            }
        } else if(size < Byte.MAX_VALUE && size > Byte.MIN_VALUE) {
            mv.visitIntInsn(BIPUSH, size);
        } else if(size < Short.MAX_VALUE && size > Short.MIN_VALUE) {
            mv.visitIntInsn(SIPUSH, size);
        } else {
            mv.visitLdcInsn(size);
        }
        // create array
        if(this != OBJECT) {
            mv.visitIntInsn(NEWARRAY, this.arrayType);
        } else {
            mv.visitTypeInsn(ANEWARRAY, nullOrClassNameOrGarbage);
        }
    }

    private AsmPrimitive(Class<?> clazz, Consumer<MethodVisitor> doReturn, BiConsumer<Stack,MethodVisitor> touchArray, TriConsumer<Stack,MethodVisitor,Integer> touchVar, int arrayOpcode, Consumer<MethodVisitor> loadDefaultValue) {
        primitives.put(clazz, this);
        this.clazz = clazz;
        this.doReturn = doReturn;
        this.touchArray = touchArray;
        this.touchVar = touchVar;
        this.arrayType = arrayOpcode;
        this.loadDefaultValue = loadDefaultValue;
    }

    public static AsmPrimitive get(Class<?> clazz) {
        return primitives.getOrDefault(clazz, OBJECT);
    }

    public static AsmPrimitive CHAR = new AsmPrimitive(
        char.class, 
        (mv)->mv.visitInsn(AsmUtil.IRETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.CALOAD : AsmUtil.CASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.ILOAD : AsmUtil.ISTORE, i),
        AsmUtil.T_CHAR,
        (mv)->mv.visitInsn(AsmUtil.ICONST_M1)
    );
    public static AsmPrimitive BYTE = new AsmPrimitive(
        byte.class, 
        (mv)->mv.visitInsn(AsmUtil.IRETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.BALOAD : AsmUtil.BASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.ILOAD : AsmUtil.ISTORE, i),
        AsmUtil.T_BYTE,
        (mv)->mv.visitInsn(AsmUtil.ICONST_M1)
    );
    public static AsmPrimitive SHORT = new AsmPrimitive(
        short.class, 
        (mv)->mv.visitInsn(AsmUtil.IRETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.SALOAD : AsmUtil.SASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.ILOAD : AsmUtil.ISTORE, i),
        AsmUtil.T_SHORT,
        (mv)->mv.visitInsn(AsmUtil.ICONST_M1)
    );
    public static AsmPrimitive INT = new AsmPrimitive(
        int.class, 
        (mv)->mv.visitInsn(AsmUtil.IRETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.IALOAD : AsmUtil.IASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.ILOAD : AsmUtil.ISTORE, i),
        AsmUtil.T_INT,
        (mv)->mv.visitInsn(AsmUtil.ICONST_M1)
    );
    public static AsmPrimitive LONG = new AsmPrimitive(
        long.class, 
        (mv)->mv.visitInsn(AsmUtil.LRETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.LALOAD : AsmUtil.LASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.LLOAD : AsmUtil.LSTORE, i),
        AsmUtil.T_LONG,
        (mv)->mv.visitInsn(AsmUtil.LCONST_0)
    );
    public static AsmPrimitive FLOAT = new AsmPrimitive(
        float.class, 
        (mv)->mv.visitInsn(AsmUtil.FRETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.FALOAD : AsmUtil.FASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.FLOAD : AsmUtil.FSTORE, i),
        AsmUtil.T_FLOAT,
        (mv)->mv.visitInsn(AsmUtil.FCONST_0)
    );
    public static AsmPrimitive DOUBLE = new AsmPrimitive(
        double.class, 
        (mv)->mv.visitInsn(AsmUtil.DRETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.DALOAD : AsmUtil.DASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.DLOAD : AsmUtil.DSTORE, i),
        AsmUtil.T_DOUBLE,
        (mv)->mv.visitInsn(AsmUtil.DCONST_0)
    );
    public static AsmPrimitive BOOL = new AsmPrimitive(
        boolean.class, 
        (mv)->mv.visitInsn(AsmUtil.IRETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.BALOAD : AsmUtil.BASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.ILOAD : AsmUtil.ISTORE, i),
        AsmUtil.T_BOOLEAN,
        (mv)->mv.visitInsn(AsmUtil.ICONST_0)
    );
    public static AsmPrimitive OBJECT = new AsmPrimitive(
        Object.class, 
        (mv)->mv.visitInsn(AsmUtil.ARETURN), 
        (stack,mv)->mv.visitInsn(stack == Stack.LOAD ? AsmUtil.AALOAD : AsmUtil.AASTORE), 
        (stack,mv,i)->mv.visitVarInsn(stack == Stack.LOAD ? AsmUtil.ALOAD : AsmUtil.ASTORE, i),
        -1,
        (mv)->mv.visitInsn(AsmUtil.ACONST_NULL)
    );
}