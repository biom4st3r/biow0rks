// package biom4st3r.libs.biow0rks.asm.__ignored__;

// import java.util.Set;

// import com.google.common.collect.Sets;
// import org.objectweb.asm.ClassVisitor;
// import org.objectweb.asm.Label;
// import org.objectweb.asm.MethodVisitor;
// import org.objectweb.asm.Opcodes;
// import org.objectweb.asm.Type;

// public class Double2FloatMethodVisitor extends MethodVisitor implements Opcodes {

//     public static class D2FClassVisitor extends ClassVisitor {

//         public D2FClassVisitor(ClassVisitor classVisitor) {
//             super(ASM9, classVisitor);
//         }

//         Set<String> methods = Sets.newHashSet();
//         String name;

//         @Override
//         public void visit(int version, int access, String name, String signature, String superName,
//                 String[] interfaces) {
//             // TODO Auto-generated method stub
//             this.name = name;
//             super.visit(version, access, name, signature, superName, interfaces);
//         }

//         @Override
//         public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
//                 String[] exceptions) {

//             Type[] types = Type.getArgumentTypes(descriptor);
//             Type rt = Type.getReturnType(descriptor);
//             for(int i = 0; i < types.length; i++) {
//                 if(types[i].equals(Type.getType(double.class))) {
//                     types[i] = Type.getType(float.class);
//                 }
//             }
//             if(rt.equals(Type.getType(double.class))) {
//                 rt = Type.getType(float.class);
//             }
//             if(this.name.contains("MathHelper")) {
//                 System.out.println();
//             }
//             descriptor = Type.getMethodDescriptor(rt, types);
//             String id = name+descriptor;
//             if(methods.contains(id)) return null;
//             else methods.add(id);
//             return new Double2FloatMethodVisitor(super.visitMethod(access, name, descriptor, signature, exceptions));
//         }

//     }

//     public Double2FloatMethodVisitor(MethodVisitor methodVisitor) {
//         super(ASM9, methodVisitor);
//     }

//     int offset = 0;
//     int offsetStart = -1;

//     @Override
//     public void visitLocalVariable(String name, String descriptor, String signature, Label start, Label end,
//             int index) {
//         Type t = Type.getType(descriptor);
//         if(index >= offsetStart) index-=offset;
//         if(t == Type.getType(double.class)) {
//             offset++;
//             if(offsetStart == -1) offsetStart = index+1;
//             descriptor = Type.getType(float.class).getDescriptor();
//         }
//         super.visitLocalVariable(name, descriptor, signature, start, end, index);
//     }

//     @Override
//     public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
//         if(owner.startsWith("net/minecraft")) {
//             Type[] types = Type.getArgumentTypes(descriptor);
//             Type rt = Type.getReturnType(descriptor);
//             for(int i = 0; i < types.length; i++) {
//                 if(types[i].equals(Type.getType(double.class))) {
//                     types[i] = Type.getType(float.class);
//                 }
//             }
//             if(rt.equals(Type.getType(double.class))) {
//                 rt = Type.getType(float.class);
//             }
//             descriptor = Type.getMethodDescriptor(rt, types);
//         }
        
//         super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
//     }

//     @Override
//     public void visitInsn(int opcode) {
//         switch(opcode) {
//             case I2D:
//                 opcode = I2F;
//                 break;
//             case L2D:
//                 opcode = L2F;
//                 break;
//             case D2F:
//             case F2D:
//                 opcode = NOP;
//                 break;
//             case D2I:
//                 opcode = F2I;
//                 break;
//             case D2L:
//                 opcode = F2L;
//                 break; 
//             case DALOAD:
//                 opcode = FALOAD;
//                 break;
//             case DASTORE:
//                 opcode = FASTORE;
//                 break;
//             case DREM:
//                 opcode = FREM;
//                 break;
//             case DNEG:
//                 opcode = FNEG;
//                 break;
//             case DCMPL:
//                 opcode = FCMPL;
//                 break;
//             case DCMPG:
//                 opcode = FCMPG;
//                 break;
//             case DADD:
//                 opcode = FADD;
//                 break;
//             case DSUB:
//                 opcode = FSUB;
//                 break;
//             case DMUL:
//                 opcode = FMUL;
//                 break;
//             case DDIV:
//                 opcode = FDIV;
//                 break;
//             case DRETURN:
//                 opcode = FRETURN;
//                 break;
//             case DCONST_0:
//                 opcode = FCONST_0;
//                 break;
//             case DCONST_1:
//                 opcode = FCONST_1;
//                 break;
//         }
//         super.visitInsn(opcode);
//     }

//     @Override
//     public void visitLdcInsn(Object value) {
//         if(value instanceof Double d) {
//             if(d.doubleValue() == 2.0D) {
//                 this.visitInsn(FCONST_2);
//             } else {
//                 value = (float)d.doubleValue();
//             }
//         }
//         super.visitLdcInsn(value);
//     }

//     @Override
//     public void visitIntInsn(int opcode, int operand) {
//         if(opcode == NEWARRAY && operand == T_DOUBLE) operand = T_FLOAT;
//         super.visitIntInsn(opcode, operand);
//     }

//     @Override
//     public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
//         if(Type.getType(descriptor).equals(Type.getType(double.class))) {
//             descriptor = Type.getDescriptor(float.class);
//         }
//         super.visitFieldInsn(opcode, owner, name, descriptor);
//     }

//     @Override
//     public void visitVarInsn(int opcode, int var) {
//         if(var >= this.offsetStart) var -= offset;
//         switch (opcode) {
//             case DLOAD:
//                 opcode = FLOAD;
//                 break;
//             case DSTORE:
//                 opcode = FSTORE;
//                 break;
//         }
//         super.visitVarInsn(opcode, var);
//     }
    
// }
