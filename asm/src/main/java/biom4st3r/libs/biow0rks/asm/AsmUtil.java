package biom4st3r.libs.biow0rks.asm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.function.Supplier;

import com.google.common.base.Suppliers;
import com.google.common.collect.Sets;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;

import net.fabricmc.loader.api.FabricLoader;

public final class AsmUtil implements Opcodes {
    public static String methodDesc(Class<?> rt, Class<?>... args) {
        Type[] types = new Type[args.length];
        int i = 0;
        for(Class<?> c : args) {
            types[i++] = Type.getType(c);
        }
        return Type.getMethodDescriptor(Type.getType(rt), types);
    }

    public static void saveBytesToFile(String name, byte[] b) {
        File file = new File(FabricLoader.getInstance().getConfigDir().toFile(), name+".class");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(file);
            fos.write(b);
            fos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String descFromClass(Class<?> clazz, boolean isArray) {
        StringBuilder builder = new StringBuilder();
        if(isArray) builder.append('[');
        switch(clazz.getName()) {
            case "int":
                builder.append('I');
                break;
            case "long":
                builder.append('J');
                break;
            case "float":
                builder.append('F');
                break;
            case "double":
                builder.append('D');
                break;
            case "char":
                builder.append('C');
                break;
            case "byte":
                builder.append('B');
                break;
            case "short":
                builder.append('S');
                break;
            case "boolean":
                builder.append('Z');
                break;
            default:
                builder.append(Type.getDescriptor(clazz));
                break;
        }
        return builder.toString();
    }

    static Set<Class<?>> prims = Sets.newHashSet(int.class,long.class,float.class,double.class,char.class,byte.class,short.class,boolean.class);

    public static Set<Class<?>> getPrims() {
        return prims;
    }

    public static boolean isPrimitive(Class<?> clazz) {
        return prims.contains(clazz);
    }

    public static int getArrayOpcode(Class<?> clazz) {
        return descFromClass(clazz, false).startsWith("L") ? ANEWARRAY : NEWARRAY;
    }

    private static interface Loader {
        Class<?> loadClass(byte[] b);
    }

    private static class _ClassLoader extends ClassLoader implements Loader {
        @Override
        public Class<?> loadClass(byte[] b) {
            return this.defineClass(null, b, 0, b.length);
        }
    }

    private static final Supplier<Loader> _classLoader = Suppliers.memoize(() -> new _ClassLoader() {});

    public static Class<?> loadClass(byte[] clazz) {
        return _classLoader.get().loadClass(clazz);
    }

    public static final Number insnToNumber(AbstractInsnNode node) {
        int i = node.getOpcode();
        switch(i) {
            case ICONST_M1:
            return -1;
            case ICONST_0:
            return 0;
            case ICONST_1:
            return 1;
            case ICONST_2:
            return 2;
            case ICONST_3:
            return 3;
            case ICONST_4:
            return 4;
            case ICONST_5:
            return 5;
            case LCONST_0:
            return 0L;
            case LCONST_1:
            return 1L;
            case DCONST_0:
            return 0.0D;
            case DCONST_1:
            return 1.0D;
            case FCONST_0:
            return 0.0F;
            case FCONST_1:
            return 1.0F;
            case FCONST_2:
            return 2.0F;
            case BIPUSH:
            return ((IntInsnNode)node).operand;
            case SIPUSH:
            return ((IntInsnNode)node).operand;
            case LDC:
            return (Number) ((LdcInsnNode)node).cst;
        }
        return 0xFFFFFFFFFFFFFFFFL;
    }

    public static ClassReader getReader(String s) {
        try {
            return new ClassReader(s);
        } catch (IOException e) {
            System.out.println(s);
            throw new RuntimeException(e);
        }
    }
}
