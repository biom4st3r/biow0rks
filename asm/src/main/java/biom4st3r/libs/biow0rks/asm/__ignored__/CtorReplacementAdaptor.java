package biom4st3r.libs.biow0rks.asm.__ignored__;
// package biom4st3r.libs.biow0rks.__ignored__;

// // import biom4st3r.libs.biow0rks.Biow0rksPL;
// import org.objectweb.asm.ClassVisitor;
// import org.objectweb.asm.MethodVisitor;
// import org.objectweb.asm.Opcodes;

// public final class CtorReplacementAdaptor extends ClassVisitor {
//     private String targetClass;
//     private String replacingClass;

//     public CtorReplacementAdaptor(ClassVisitor classVisitor, String targetClass, String replacingClass) {
//         super(Opcodes.ASM9, classVisitor);
//         this.targetClass = targetClass;
//         this.replacingClass = replacingClass;
//     }

//     @Override
//     public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
//         if(superName.equals(targetClass)) superName = replacingClass;
//         super.visit(version, access, name, signature, superName, interfaces);
//     }

//     @Override
//     public MethodVisitor visitMethod(int access, String methodName, String descriptor, String signature,
//             String[] exceptions) {
//         return new MethodVisitor(Biow0rksPL.ASM9, super.visitMethod(access, methodName, descriptor, signature, exceptions)) {

//             @Override
//             public void visitTypeInsn(int opcode, String type) {
//                 if(type.equals(targetClass)) {
//                     type = replacingClass;
//                 }
//                 super.visitTypeInsn(opcode, type);
//             }
//             @Override
//             public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
//                 if(opcode == Biow0rksPL.INVOKESPECIAL && owner.equals(targetClass)) owner = replacingClass;
//                 super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
//             }
//         };
//     }
// }