package biom4st3r.libs.biow0rks.asm;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

public final class LoadLessClassWriter extends ClassWriter {
    public LoadLessClassWriter(int flags) {
        super(flags);
    }

    // @Override
    // protected ClassLoader getClassLoader() {
    //     return Smile.knot;
    // }

    @Override
    protected String getCommonSuperClass(String type1, String type2) {
        // TODO replace
        return getCommonSuperClass0(type1, type2);
    }
    // ~~~~~~From ASMR~~~~~~~ //

    public static String getCommonSuperClass0(String type1, String type2) {
        if (type1 == null || type2 == null) {
            return "java/lang/Object";
        }

        if (isDerivedFrom(type1, type2)) {
            return type2;
        } else if (isDerivedFrom(type2, type1)) {
            return type1;
        } else if (getClassInfo(type1).isInterface || getClassInfo(type2).isInterface) {
            return "java/lang/Object";
        }

        do {
            type1 = getClassInfo(type1).superClass;
            if (type1 == null) {
                return "java/lang/Object";
            }
        } while (!isDerivedFrom(type2, type1));

        return type1;
    }

    private static record ClassInfo(String superClass, boolean isInterface) {

    }

    private static boolean isDerivedFrom(String subtype, String supertype) {
        subtype = getClassInfo(subtype).superClass;

        Set<String> visitedTypes = new HashSet<>();

        while (subtype != null) {
            if (!visitedTypes.add(subtype)) {
                return false;
            }
            if (supertype.equals(subtype)) {
                return true;
            }
            subtype = getClassInfo(subtype).superClass;
        }

        return false;
    }

    private static final ConcurrentHashMap<String, ClassInfo> classInfoCache = new ConcurrentHashMap<>();

    private static ClassInfo getClassInfo(String type) {
        return classInfoCache.computeIfAbsent(type, className -> {

            ClassReader cr = null;
            try {
                // cr = new ClassReader(knot.getResourceAsStream(className.replace('.', '/') + ".class"));
                cr = new ClassReader(LoadLessClassWriter.class.getClassLoader().getResourceAsStream(className.replace('.', '/') + ".class"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return new ClassInfo(cr.getSuperName(), (cr.getAccess() & Opcodes.ACC_INTERFACE) != 0);
        });
    }
}