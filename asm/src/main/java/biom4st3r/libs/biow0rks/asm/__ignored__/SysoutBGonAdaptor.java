// package biom4st3r.libs.biow0rks.asm.__ignored__;

// import java.io.PrintStream;

// import org.objectweb.asm.ClassVisitor;
// import org.objectweb.asm.MethodVisitor;
// import org.objectweb.asm.Opcodes;
// import org.objectweb.asm.Type;

// public class SysoutBGonAdaptor extends ClassVisitor {

//     public SysoutBGonAdaptor(ClassVisitor classVisitor) {
//         super(Opcodes.ASM9, classVisitor);
//     }
//     @Override
//     public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
//             String[] exceptions) {
//         MethodVisitor mv = super.visitMethod(access, name, descriptor, signature, exceptions);
//         // MethodNode node = new MethodNode() {
//         //     @Override
//         //     public void visitEnd() {
//         //         FieldInsnNode system_out = null;
//         //         MethodInsnNode print = null;
//         //         for (int i = 0; i < this.instructions.size(); i++) {
//         //             AbstractInsnNode insn = this.instructions.get(i);
//         //             if(insn instanceof FieldInsnNode fn) {
//         //                 if(fn.owner.equals("java/lang/System") && fn.name.equals("out")) {
//         //                     system_out = fn;
//         //                 }
//         //             } else if(insn instanceof MethodInsnNode mn) {
//         //                 if(mn.owner.equals(Type.getInternalName(PrintStream.class)) && mn.name.startsWith("print")) {
//         //                     print = mn;
//         //                 }
//         //             }
//         //             if(system_out != null && print != null) {
//         //                 this.instructions.remove(system_out);
//         //                 Type[] args = Type.getArgumentTypes(print.desc);
//         //                 if(args.length > 0) {
//         //                     if(args[0].equals(Type.getType(double.class)) || args[0].equals(Type.getType(long.class))) {
//         //                         this.instructions.set(print, new InsnNode(Opcodes.POP2));
//         //                     } else {
//         //                         this.instructions.set(print, new InsnNode(Opcodes.POP));
//         //                     }
//         //                 } else {
//         //                     this.instructions.remove(print);
//         //                 }
//         //                 system_out = null;
//         //                 print = null;
//         //             }
//         //         }
//         //         super.visitEnd();
//         //     }
//         // };

//         // return node;
//         return new MethodVisitor(Opcodes.ASM9,mv) {
//             @Override
//             public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
//                 if(owner.equals(Type.getInternalName(PrintStream.class)) && name.startsWith("print")) {
//                     Type[] types = Type.getArgumentTypes(descriptor);
//                     // if(descriptor.equals("()V")) {
//                     //     this.visitInsn(Opcodes.POP);
//                     //     return;
//                     // }
//                     if(types.length == 1) {
//                         if(types[0].equals(Type.getType(long.class)) || types[0].equals(Type.getType(double.class))) {
//                             this.visitInsn(Opcodes.POP2);
//                         } else {
//                             this.visitInsn(Opcodes.POP);
//                         }
//                     }
                    
//                     this.visitInsn(Opcodes.POP);
//                 } else {
//                     super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
//                 }
//             }
//             // @Override
//             // public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
//             //     if(opcode == Opcodes.GETSTATIC && owner.equals("java/lang/System") && name.equals("out")) {
//             //         return;
//             //     }
//             //     super.visitFieldInsn(opcode, owner, name, descriptor);
//             // }
//         };
//     }
// }
