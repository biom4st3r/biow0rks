package biom4st3r.libs.biow0rks;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Spliterator;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collector;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.Internal;

@AvailableSince("0.1.0")
public class StreamImpl<T> implements Stream<T> {
    private static @interface Intermediate {}
    private static @interface Terminal {}

    private Object[] container;
    private Runnable closeHandler;
    private Queue<Runnable> INTERMEDIATE_OPS = Queues.newArrayDeque();

    public StreamImpl(T[] ts) {
        this.container = Arrays.copyOf(ts, ts.length);
    }

    public StreamImpl(Iterable<T> itab) {
        this(itab.iterator());
    }

    public StreamImpl(Iterator<T> iter) {
        container = new Object[16];
        int size = 0;
        while(iter.hasNext()) {
            if (size == container.length) {
                container = Arrays.copyOf(container, container.length+16);
            }
            container[size] = iter.next();
            size++;
        }
        if (size != container.length) {
            container = Arrays.copyOf(container, size);
        }
    }

    public StreamImpl(Collection<T> coll) {
        this.container = coll.toArray(Object[]::new);
    }

    @Internal
    @SuppressWarnings({"unchecked"})
    private T get(int i) {
        return (T) this.container[i];
    }

    @Internal
    private void runIntermediates() {
        while(!this.INTERMEDIATE_OPS.isEmpty()) {
            this.INTERMEDIATE_OPS.poll().run();
        }
    }

    @Override
    @Terminal
    public Iterator<T> iterator() {
        this.runIntermediates();
        this.close();
        return new Iterator<T>(){
            int i = 0;
            @Override
            public boolean hasNext() {
                return i < container.length;
            }

            @Override
            public T next() {
                return get(i++);
            }
        };
    }

    @Override
    @Terminal
    @SuppressWarnings({"unchecked"})
    public Spliterator<T> spliterator() {
        this.runIntermediates();
        Spliterator<T> sit = (Spliterator<T>) Arrays.spliterator(this.container);
        this.close();
        return sit;
    }

    @Override
    public boolean isParallel() {
        return false;
    }

    @Override
    @Intermediate
    public Stream<T> sequential() {
        return this;
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public Stream<T> parallel() {
        // Technically incorrect
        this.runIntermediates();
        Stream<T> st = (Stream<T>) Stream.of(this.container).parallel();
        this.close();
        return st;
    }

    @Override
    @Intermediate
    public Stream<T> unordered() {
        return this;
    }

    @Override
    @Intermediate
    public Stream<T> onClose(Runnable closeHandler) {
        this.closeHandler = closeHandler;
        return this;
    }

    @Override
    public void close() {
        if(this.closeHandler != null) this.closeHandler.run();
        this.closeHandler = null;
    }

    @Override
    @Intermediate
    public Stream<T> filter(Predicate<? super T> predicate) {
        this.INTERMEDIATE_OPS.offer(()-> {
            Object[] newT = new Object[this.container.length];
            int newTi = 0;
            for(int i = 0 ;i < newT.length;i++) {
                if(predicate.test(get(i))) {
                    newT[newTi++] = get(i);
                }
            }
            this.container = newT;
        });
        return this;
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public <R> Stream<R> map(Function<? super T, ? extends R> mapper) {
        this.INTERMEDIATE_OPS.offer(()-> {
            for(int i = 0; i < this.container.length; i++) this.container[i] = mapper.apply(this.get(i));
        });
        return (Stream<R>) this;
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public IntStream mapToInt(ToIntFunction<? super T> mapper) {
        // Technically incorrect
        this.runIntermediates();
        
        java.util.stream.IntStream.Builder builder = IntStream.builder();
        for(Object t : this.container) builder.add(mapper.applyAsInt((T) t));
        this.close();
        return builder.build();
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public LongStream mapToLong(ToLongFunction<? super T> mapper) {
        // Technically incorrect
        this.runIntermediates();
        java.util.stream.LongStream.Builder builder = LongStream.builder();
        for(Object t : this.container) builder.add(mapper.applyAsLong((T) t));
        this.close();
        return builder.build();
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public DoubleStream mapToDouble(ToDoubleFunction<? super T> mapper) {
        // Technically incorrect
        this.runIntermediates();
        java.util.stream.DoubleStream.Builder builder = DoubleStream.builder();
        for(Object t : this.container) builder.add(mapper.applyAsDouble((T) t));
        this.close();
        return builder.build();
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public <R> Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper) {
        this.INTERMEDIATE_OPS.offer(()-> {
            List<R> list = Lists.newArrayList();
            for(Object t : this.container) mapper.apply((T) t).forEach(list::add);
            this.container = list.toArray(Object[]::new);
        });
        return (Stream<R>) this;
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public IntStream flatMapToInt(Function<? super T, ? extends IntStream> mapper) {
        // Technically incorrect
        this.runIntermediates();
        IntStream.Builder builder = IntStream.builder();
        for(Object t : this.container) mapper.apply((T) t).forEach(builder::add);
        this.close();
        return builder.build();
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public LongStream flatMapToLong(Function<? super T, ? extends LongStream> mapper) {
        // Technically incorrect
        this.runIntermediates();
        LongStream.Builder builder = LongStream.builder();
        for(Object t : this.container) mapper.apply((T) t).forEach(builder::add);
        this.close();
        return builder.build();
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public DoubleStream flatMapToDouble(Function<? super T, ? extends DoubleStream> mapper) {
        // Technically incorrect
        this.runIntermediates();
        DoubleStream.Builder builder = DoubleStream.builder();
        for(Object t : this.container) mapper.apply((T) t).forEach(builder::add);
        this.close();
        return builder.build();
    }

    @Override
    @Intermediate
    public Stream<T> distinct() {
        this.INTERMEDIATE_OPS.offer(()-> {
            Object[] newT = new Object[this.container.length];
            int index = 0;
            main:
            for(int i = 0; i < this.container.length; i++) {
                for(int j = 0; j < this.container.length; j++) {
                    if(j == i) continue;
                    if(this.container[i].equals(this.container[j])) {
                        continue main;
                    }
                }
                newT[index++] = this.container[i];
            }
            this.container = Arrays.copyOf(newT, index);
        });
        return this;
    }

    @Override
    @Intermediate
    public Stream<T> sorted() {
        return this;
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public Stream<T> sorted(Comparator<? super T> comparator) {
        this.INTERMEDIATE_OPS.offer(()->Arrays.sort(this.container, (Comparator<Object>)comparator));
        return this;
    }

    @Override
    @Intermediate
    @SuppressWarnings({"unchecked"})
    public Stream<T> peek(Consumer<? super T> action) {
        this.INTERMEDIATE_OPS.offer(()-> {
            for(Object t : this.container) {
                action.accept((T) t);
            }
        });
        return this;
    }

    @Override
    @Intermediate
    public Stream<T> limit(long maxSize) {
        this.INTERMEDIATE_OPS.offer(()->this.container = Arrays.copyOf(this.container, (int)Math.min(maxSize, this.container.length)));
        return this;
    }

    @Override
    @Intermediate
    public Stream<T> skip(long n) {
        this.INTERMEDIATE_OPS.offer(()->this.container = Arrays.copyOfRange(this.container, (int)n, this.container.length));
        return this;
    }

    @Override
    @Terminal
    @SuppressWarnings({"unchecked"})
    public void forEach(Consumer<? super T> action) {
        this.runIntermediates();
        for(Object t : this.container) {
            action.accept((T) t);
        }
        this.close();
    }

    @Override
    @Terminal
    public void forEachOrdered(Consumer<? super T> action) {
        this.runIntermediates();
        this.forEach(action);
        this.close();
    }

    @Override
    @Terminal
    public Object[] toArray() {
        this.runIntermediates();
        Object[] objs = Arrays.copyOf(this.container, this.container.length);
        this.close();
        return objs;
    }

    @Override
    @Terminal
    public <A> A[] toArray(IntFunction<A[]> generator) {
        this.runIntermediates();
        A[] array = Arrays.copyOf(generator.apply(0), this.container.length);
        System.arraycopy(this.container, 0, array, 0, this.container.length);
        this.close();
        return array;
    }

    @Override
    @Terminal
    @SuppressWarnings({"unchecked"})
    public T reduce(T identity, BinaryOperator<T> accumulator) {
        this.runIntermediates();
        T result = identity;
        for(Object t : this.container) {
            result = accumulator.apply(result, (T) t);
        }
        this.close();
        return result;
    }

    @Override
    @Terminal
    public Optional<T> reduce(BinaryOperator<T> accumulator) {
        this.runIntermediates();
        T result = this.get(0);
        for(int i = 1; i < this.container.length; i++) {
            result = accumulator.apply(result, get(i));
        }
        this.close();
        return Optional.of(result);
    }

    @Override
    @Terminal
    @SuppressWarnings({"unchecked"})
    public <U> U reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner) {
        this.runIntermediates();
        U result = identity;
        for(Object t : this.container) {
            result = accumulator.apply(result, (T) t);
        }
        this.close();
        return result;
    }

    @Override
    @Terminal
    @SuppressWarnings({"unchecked"})
    public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator, BiConsumer<R, R> combiner) {
        this.runIntermediates();
        R r = supplier.get();
        for(Object t : this.container) {
            accumulator.accept(r, (T) t);
        }
        this.close();
        return r;
    }

    @Override
    @Terminal
    @SuppressWarnings({"unchecked"})
    public <R, A> R collect(Collector<? super T, A, R> collector) {
        this.runIntermediates();
        A a = collector.supplier().get();
        for(Object t : this.container) {
            collector.accumulator().accept(a, (T) t);
            
        }
        R r = collector.finisher().apply(a);
        this.close();
        return r;
    }

    @Override
    @Terminal
    public Optional<T> min(Comparator<? super T> comparator) {
        this.runIntermediates();
        if(this.container.length == 0) return Optional.empty();
        this.sorted(comparator);
        Optional<T> ot = Optional.of(this.get(0));
        this.close();
        return ot;
    }

    @Override
    @Terminal
    public Optional<T> max(Comparator<? super T> comparator) {
        this.runIntermediates();
        if(this.container.length == 0) return Optional.empty();
        this.sorted(comparator);
        Optional<T> ot = Optional.of(this.get(this.container.length-1));
        this.close();
        return ot;
    }

    @Override
    @Terminal
    public long count() {
        this.runIntermediates();
        return this.container.length;
    }

    @Override
    @Terminal
    public boolean anyMatch(Predicate<? super T> predicate) {
        this.runIntermediates();
        for(int i = 0; i < this.container.length; i++) {
            if(predicate.test(get(i))) return true;
        }
        return false;
    }

    @Override
    @Terminal
    @SuppressWarnings({"unchecked"})
    public boolean allMatch(Predicate<? super T> predicate) {
        this.runIntermediates();
        boolean result = true;
        for(Object t : this.container) {
            result &= predicate.test((T) t);
            if(!result) return false;
        }
        return result;
    }

    @Override
    @Terminal
    @SuppressWarnings({"unchecked"})
    public boolean noneMatch(Predicate<? super T> predicate) {
        this.runIntermediates();
        boolean result = false;
        for(Object t : this.container) {
            result |= predicate.test((T) t);
        }
        return !result;
    }

    @Override
    @Terminal
    public Optional<T> findFirst() {
        this.runIntermediates();
        Optional<T> ot = Optional.of(this.get(0));
        this.close();
        return ot;
    }

    @Override
    @Terminal
    public Optional<T> findAny() {
        this.runIntermediates();
        return findFirst();
    }
    
}
