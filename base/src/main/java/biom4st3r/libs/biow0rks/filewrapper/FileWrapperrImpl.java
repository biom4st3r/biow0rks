package biom4st3r.libs.biow0rks.filewrapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.function.Function;
import java.util.stream.Stream;

class FileWrapperrImpl implements FileWrapper,FileWrapper$File,FileWrapper$Folder {

    private final File file;
    private boolean mkdirs = false;

    FileWrapperrImpl(File f) {
        this.file = f;
    }

    private void _mkdirs() {
        if (!this.mkdirs) {
            this.file.mkdirs();
            this.mkdirs = true;
        }
    }

    public OperationResult writeToFile(String s, boolean append) {
        if(notFileOP()) return OperationResult.FILE_OP_ON_FOLDER;
        return this.writeToFile(s, append);
    }

    @Override
    public FileWrapper$File asFile() {
        return this;
    }

    @Override
    public FileWrapper$Folder asFolder() {
        return this;
    }

    private boolean notFileOP() {
        _mkdirs();
        return !this.file.isFile();
    }

    private boolean notFolderOP() {
        _mkdirs();
        return !this.file.isDirectory();
    }

    @Override
    public boolean exists() {
        return this.file.exists();
    }

    @Override
    public <T> Result<T> readerAdapter(Function<Reader, T> func) {
        if (notFileOP()) return Result.err(OperationResult.FILE_OP_ON_FOLDER);
        if (!this.file.exists()) return Result.err(OperationResult.NOT_FOUND);
        try (FileReader reader = new FileReader(this.file)) {
            T t = func.apply(reader);
            return Result.ok(t);
        } catch (IOException e) {
            // e.printStackTrace();
            return Result.err(OperationResult.ERROR, e);
        }
    }

    @Override
    public <T> Result<T> writeAdapter(boolean append, Function<Writer, T> func) {
        if(notFileOP()) return Result.err(OperationResult.FILE_OP_ON_FOLDER);
        try (FileWriter writer = new FileWriter(this.file, append)) {
            T t = func.apply(writer);
            return Result.ok(t);
        } catch (IOException e) {
            return Result.err(OperationResult.ERROR, e);
        }
    }

    @Override
    public OperationResult writeToFile(byte[] b, boolean append) {
        if (notFileOP()) return OperationResult.FILE_OP_ON_FOLDER;
        FileOutputStream stream;
        // if (!this.file.exists()) return FileResult.NOT_FOUND;
        try {
            stream = new FileOutputStream(this.file, append);
            stream.write(b);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return OperationResult.ERROR;
        }
        return OperationResult.SUCCESS;
    }

    public Result<File[]> getFiles() {
        if(this.notFolderOP()) return Result.err(OperationResult.FOLDER_OP_ON_FILE);
        return Result.ok(this.file.listFiles());
    }

    public Result<FileWrapper[]> getWrappedFiles() {
        if(this.notFolderOP()) return Result.err(OperationResult.FOLDER_OP_ON_FILE);
        return Result.ok(Stream.of(this.file.listFiles()).map(f -> new FileWrapperrImpl(f)).toArray(FileWrapper[]::new));
    }

    public Result<Stream<FileWrapper>> wrappedStream() {
        if(this.notFolderOP()) return Result.err(OperationResult.FOLDER_OP_ON_FILE);
        return Result.ok(Stream.of(this.file.listFiles()).map(f->FileWrapper.of(f)));
    }

    @Override
    public Result<byte[]> read() {
        if(this.notFileOP()) return Result.err(OperationResult.FILE_OP_ON_FOLDER);
        try {
            FileInputStream i = new FileInputStream(file);
            byte[] array = i.readAllBytes();
            i.close();
            return Result.ok(array);
        } catch (RuntimeException | IOException exception) {
            return Result.err(OperationResult.ERROR, exception);
        }
    }

}