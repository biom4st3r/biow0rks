package biom4st3r.libs.biow0rks.filewrapper;

import java.io.File;
import java.util.stream.Stream;

public interface FileWrapper$Folder {
    Result<File[]> getFiles();
    Result<FileWrapper[]> getWrappedFiles();
    Result<Stream<FileWrapper>> wrappedStream();
}