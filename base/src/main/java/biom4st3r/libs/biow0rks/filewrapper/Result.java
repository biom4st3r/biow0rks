package biom4st3r.libs.biow0rks.filewrapper;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public record Result<T>(T get, OperationResult err, Exception e) {

    public boolean isErr() {
        return !this.err.isSuccess();
    }

    public static <T> Result<T> err(OperationResult result) {
        return new Result<T>(null, result, null);
    }

    public static <T> Result<T> err(OperationResult result, Exception e) {
        return new Result<T>(null, result, e);
    }

    public static <T> Result<T> ok(T result) {
        return new Result<T>(result, OperationResult.SUCCESS, null);
    }

    public T getOrElse(T t) {
        if (isErr()) return t;
        return get;
    }

    public T getOrElse(Supplier<T> t) {
        if (isErr()) return t.get();
        return get;
    }

    public T getOrThrow() {
        throwException();
        return get;
    }

    public void throwException() {
        if (isErr() && e != null) {
            throw new RuntimeException(e);
        } else if(isErr()) {
            throw new RuntimeException(err.name());
        }
    }

    public void withValue(Consumer<T> consumer) {
        if (this.isErr()) {
            FileWrapper.logger.error(this.err.toString());
            if (this.e() != null) e.printStackTrace();
            return;
        }
        if (!this.isErr()) consumer.accept(this.get());
    }

    public void printErr() {
        if (this.isErr()) {
            FileWrapper.logger.error(this.err.toString());
            if (this.e() != null) e.printStackTrace();
            return;
        }
    }

    public <R> R adaptValue(Function<T,R> func) {
        if (this.isErr()) {
            FileWrapper.logger.error(this.err.toString());
            if (this.e() != null) e.printStackTrace();
            return null;
        }
        return func.apply(this.get());
    }
}