package biom4st3r.libs.biow0rks.filewrapper;

public enum OperationResult {
    SUCCESS,
    ERROR,
    NOT_FOUND,
    FILE_OP_ON_FOLDER,
    FOLDER_OP_ON_FILE,
    ;
    public boolean isSuccess() {
        return this == OperationResult.SUCCESS;
    }
}