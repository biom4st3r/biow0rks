package biom4st3r.libs.biow0rks.filewrapper;

import java.io.Reader;
import java.io.Writer;
import java.util.function.Function;

public interface FileWrapper$File {
    <T> Result<T> readerAdapter(Function<Reader, T> func);
    <T> Result<T> writeAdapter(boolean append, Function<Writer, T> func);
    OperationResult writeToFile(byte[] b, boolean append);
    Result<byte[]> read();
}