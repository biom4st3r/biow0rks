package biom4st3r.libs.biow0rks.filewrapper;

import java.io.File;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import biom4st3r.libs.biow0rks.BioLogger;

@AvailableSince("0.2.3")
public interface FileWrapper {

    public static FileWrapper of(String s) {
        return of(new File(s));
    }

    public static FileWrapper of(File f) {
        return new FileWrapperrImpl(f);
    }

    static final BioLogger logger = new BioLogger("FileWrapper");
    FileWrapper$File asFile();
    FileWrapper$Folder asFolder();

    boolean exists();
}
