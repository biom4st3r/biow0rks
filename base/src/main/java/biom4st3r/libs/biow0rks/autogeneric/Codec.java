package biom4st3r.libs.biow0rks.autogeneric;

public interface Codec<GenericBase, ListLike extends GenericBase, KeyedListLike extends GenericBase> {
    public <T extends GenericBase> T sBoolean(boolean b);
    public <T extends GenericBase> T sDouble(double b);
    public <T extends GenericBase> T sFloat(float b);
    public <T extends GenericBase> T sByte(byte b);
    public <T extends GenericBase> T sInt(int b);
    public <T extends GenericBase> T sLong(long b);
    public <T extends GenericBase> T sString(String b);

    public <T extends GenericBase> boolean dsBoolean(T b);
    public <T extends GenericBase> double dsDouble(T b);
    public <T extends GenericBase> float dsFloat(T b);
    public <T extends GenericBase> byte dsByte(T b);
    public <T extends GenericBase> int dsInt(T b);
    public <T extends GenericBase> long dsLong(T b);
    public <T extends GenericBase> String dsString(T b);

    public ListLike ListLike_new();
    public <T extends GenericBase> void ListLike_add(ListLike t, T obj);
    public int ListLike_size(ListLike like);
    public <T extends GenericBase> T ListLike_get(ListLike list, int i);

    public KeyedListLike KeyedList_new();
    public int KeyedList_size(KeyedListLike keyed);
    public <T extends GenericBase> void KeyedList_add(KeyedListLike t, String key, T obj);
    public <T extends GenericBase> T KeyedList_get(KeyedListLike t, String key);
    public boolean KeyedList_contains(KeyedListLike keyed, String s);
}
