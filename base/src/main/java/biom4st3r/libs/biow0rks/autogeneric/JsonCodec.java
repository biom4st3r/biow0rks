package biom4st3r.libs.biow0rks.autogeneric;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

@SuppressWarnings({"unchecked"})
public class JsonCodec implements Codec<JsonElement, JsonArray, JsonObject> {

    @Override
    public <T extends JsonElement> T sBoolean(boolean b) {
        return (T) new JsonPrimitive(b);
    }

    @Override
    public <T extends JsonElement> T sDouble(double b) {
        return (T) new JsonPrimitive(b);
    }

    @Override
    public <T extends JsonElement> T sFloat(float b) {
        return (T) new JsonPrimitive(b);
    }

    @Override
    public <T extends JsonElement> T sByte(byte b) {
        return (T) new JsonPrimitive(b);
    }

    @Override
    public <T extends JsonElement> T sInt(int b) {
        return (T) new JsonPrimitive(b);
    }

    @Override
    public <T extends JsonElement> T sLong(long b) {
        return (T) new JsonPrimitive(b);
    }

    @Override
    public <T extends JsonElement> T sString(String b) {
        return (T) new JsonPrimitive(b);
    }

    @Override
    public <T extends JsonElement> boolean dsBoolean(T b) {
        return b.getAsBoolean();
    }

    @Override
    public <T extends JsonElement> double dsDouble(T b) {
        return b.getAsDouble();
    }

    @Override
    public <T extends JsonElement> float dsFloat(T b) {
        return b.getAsFloat();
    }

    @Override
    public <T extends JsonElement> byte dsByte(T b) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public <T extends JsonElement> int dsInt(T b) {
        return b.getAsInt();
    }

    @Override
    public <T extends JsonElement> long dsLong(T b) {
        return b.getAsLong();
    }

    @Override
    public <T extends JsonElement> String dsString(T b) {
        return b.getAsString();
    }

    @Override
    public JsonArray ListLike_new() {
        return new JsonArray();
    }

    @Override
    public <T extends JsonElement> void ListLike_add(JsonArray t, T obj) {
        t.add(obj);
    }

    @Override
    public int ListLike_size(JsonArray like) {
        return like.size();
    }

    @Override
    public <T extends JsonElement> T ListLike_get(JsonArray list, int i) {
        return (T) list.get(i);
    }

    @Override
    public JsonObject KeyedList_new() {
        return new JsonObject();
    }

    @Override
    public int KeyedList_size(JsonObject keyed) {
        return keyed.size();
    }

    @Override
    public <T extends JsonElement> void KeyedList_add(JsonObject t, String key, T obj) {
        t.add(key, obj);
    }

    @Override
    public <T extends JsonElement> T KeyedList_get(JsonObject t, String key) {
        return (T) t.get(key);
    }

    @Override
    public boolean KeyedList_contains(JsonObject keyed, String s) {
        return keyed.has(s);
    }
}
