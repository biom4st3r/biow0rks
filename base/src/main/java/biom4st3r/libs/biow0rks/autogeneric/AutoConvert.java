package biom4st3r.libs.biow0rks.autogeneric;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.RecordComponent;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.Experimental;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.Nullable;

import biom4st3r.libs.biow0rks.BioLogger;
import biom4st3r.libs.biow0rks.NoEx;
import biom4st3r.libs.biow0rks.autogeneric.HintHandler.CustomDecoder;
import biom4st3r.libs.biow0rks.autogeneric.HintHandler.CustomEncoder;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

@SuppressWarnings({"rawtypes","unchecked"})

@Experimental
@AvailableSince("0.2.4")
public class AutoConvert<GenericBase, ListLike extends GenericBase, KeyedListLike extends GenericBase> {
    private static final BioLogger logger = new BioLogger("AutoConverter");
    protected final Codec<GenericBase, ListLike, KeyedListLike> codec;
    protected final HintHandler handler = new HintHandler();

    /**
     * Determines if generated encoders and decoders get for later
     */
    public boolean cacheGenerated = true;

    @AvailableSince("0.2.6")
    public void registerCustomAnnotationHandle(String key, CustomEncoder cEncoder, CustomDecoder cDecoder) {
        handler.register(key, cEncoder, cDecoder);
    }

    public AutoConvert(Codec<GenericBase, ListLike, KeyedListLike> codec) {
        this.codec = codec;
        this.defaultInit();
    }

    public static <GenericBase, ListLike extends GenericBase, KeyedListLike extends GenericBase> AutoConvert<GenericBase, ListLike, KeyedListLike> createConverter(Codec<GenericBase, ListLike , KeyedListLike> codec) {
        return new AutoConvert<>(codec);
    }

    protected static final BioLogger LOGGER = new BioLogger("AUTO_CONVERT");
    public static final SerializeHint DEFAULT_HINTS = spoofAnnotation(null, null, null);

    @Retention(RetentionPolicy.RUNTIME)
    public static @interface SerializeHint {
        Class<?> mapKeyHint() default Void.class;
        Class<?> mapValHint() default Void.class;
        Class<?> collectionHint() default Void.class;
        boolean collectionUseLookupTable() default false;
        String custom() default "";
    }

    @Retention(RetentionPolicy.RUNTIME)
    public static @interface SkipAutoSerialize {}

    /**
     * B is purely for type hinting in the register function
     */
    public interface Encoder<OBJECT, _GenericBase> {
        _GenericBase encode(OBJECT obj, SerializeHint hints);
    }

    /**
     * B is purely for type hinting in the register function
     */
    public interface Decoder<OBJECT, _GenericBase> {
        OBJECT decode(_GenericBase tag, SerializeHint hints);
    }

    protected static boolean isPublic(Field f) {
        return Modifier.isPublic(f.getModifiers());
    }

    protected static boolean isStatic(Field f) {
        return Modifier.isStatic(f.getModifiers());
    }

    protected static boolean hasAnnotation(Field f) {
        return f.getAnnotation(SerializeHint.class) != null;
    }

    protected static SerializeHint spoofAnnotation(Class<?> mapKeyType, Class<?> mapValType, Class<?> collectionsType) {
        return new SerializeHint() {

            @Override
            public Class<? extends Annotation> annotationType() {
                return SerializeHint.class;
            }

            @Override
            public Class<?> mapKeyHint() {
                return mapKeyType == null ? Void.class : mapKeyType;
            }

            @Override
            public Class<?> mapValHint() {
                return mapValType == null ? Void.class : mapValType;
            }

            @Override
            public Class<?> collectionHint() {
                return collectionsType == null ? Void.class : collectionsType;
            }

            @Override
            public boolean collectionUseLookupTable() {
                return false;
            }

            @Override
            public String custom() {
                return "";
            }
        };
    }

    protected final Map<Class<?>,Encoder<?, ? extends GenericBase>> SERIALIZE   = Maps.newHashMap();
    protected final Map<Class<?>,Decoder<?, ? extends GenericBase>> DESERIALIZE = Maps.newHashMap();

    public <T,B extends GenericBase> void register(Class<T> t, Encoder<T,B> ser, Decoder<T,B> deser) {
        SERIALIZE.put(t, ser);
        DESERIALIZE.put(t, deser);
    }

    public <T, B extends GenericBase> void register(Class<T> t, Registry<T> registry) {
        this.register(t, (item,hints) -> serialize(registry.getId(item)), (tag,hints)->registry.get(deserialize(Identifier.class, tag)));
    }

    /**
     * Converts any object to codec
     * @param host - Object attempting to serialize
     * @return
     */
    public <T extends GenericBase> T serialize(Object host) {
        return this.serialize(host.getClass(), host, DEFAULT_HINTS);
    }

    public <T extends GenericBase> T serialize(Class<?> hostType, Object host) {
        return this.serialize(hostType, host, DEFAULT_HINTS);
    }

    public <T extends GenericBase> T serialize(Object host, @Nullable Class<?> mapKeyType, @Nullable Class<?> mapValType, Class<?> collectionsType) {
        return this.serialize(host.getClass(), host, spoofAnnotation(mapKeyType, mapValType, collectionsType));
    }

    @Internal
    protected <T extends GenericBase> T serialize(Object host, SerializeHint hints) {
        return this.serialize(host.getClass(), host, hints);
    }

    public static record SerialContext(Class<?> fieldType, String fieldName, MethodHandle handle, SerializeHint hints){}

    protected static <T> T lambdaGuard(T t) {
        return t;
    }

    public SerializeHint getOrDefault(RecordComponent component) {
        if (component.getAnnotation(SerializeHint.class) == null) {
            return DEFAULT_HINTS;
        } else {
            return component.getAnnotation(SerializeHint.class);
        }
    }

    private Encoder<? extends Record, GenericBase> makeRecordEncoder(Class<?> hostClass, Object host, SerializeHint hints) {
        final RecordComponent[] rcs = hostClass.getRecordComponents();
        final MethodHandle[] handles = Stream.of(rcs).map(rc->NoEx.run(() -> this.getLookup().unreflect(rc.getAccessor()))).toArray(MethodHandle[]::new);

        Encoder encoder = (o,_hints) -> {
            final KeyedListLike obj = this.codec.KeyedList_new();
            for (int i = 0; i < handles.length; i++) {
                final MethodHandle handle = handles[i];
                this.codec.KeyedList_add(obj, rcs[i].getName(), serialize(rcs[i].getType(), NoEx.run(() -> handle.invoke(o)), getOrDefault(rcs[i])));
            }
            return obj;
        };
        return encoder;
    }

    private void warnings(Class<?> hostClass, Object host) {
        if (List.class.isAssignableFrom(hostClass) && hostClass != List.class) {
            logger.log("A List sub-class object %s was offered to AutoConverter. Please manually specify List.class, if this wasn't intended.", host);
        }
    }

    private GenericBase runEncoder(Encoder<Object, GenericBase> encoder, Object host, SerializeHint hints) {
        CustomEncoder hint = this.handler.getEncoderHint(hints);
        if (hint != null) {
            GenericBase genericBase = hint.encode(codec, host);
            if (genericBase != null) return genericBase;
        }
        return encoder.encode(host, hints);
    }

    private Object runDecoder(Decoder<Object, GenericBase> decoder, GenericBase host, SerializeHint hints) {
        CustomDecoder hint = this.handler.getDecoderHint(hints);
        if (hint != null) {
            Object o = hint.decode(codec, host);
            if (o != null) return o;
        }
        return decoder.decode(host, hints);
    }

    public <T extends GenericBase> T serialize(Class<?> hostClass, Object host, SerializeHint hints) {
        host = lambdaGuard(host);
        // hostClass = lambdaGuard(hostClass);
        hints = lambdaGuard(hints);
        // Try to use a serializer
        warnings(hostClass, host);
        if (SERIALIZE.containsKey(hostClass)) {
            return (T) runEncoder(((Encoder)SERIALIZE.get(hostClass)), host, hints);
            // return (T) ((Encoder)SERIALIZE.get(hostClass)).encode(host, hints);
        } else if (hostClass.isRecord()) { // RECORD
            if (host == null) {
                return (T) runEncoder((a,b) -> this.codec.KeyedList_new(), host, hints);
            }
            Encoder serial = makeRecordEncoder(hostClass, host, hints);
            if (cacheGenerated) SERIALIZE.put(hostClass, serial);
            return (T) runEncoder(serial, host, hints);
            // return (T) serial.encode(host, hints);
        } else if (hostClass.isEnum()) { // ENUM
            if (host == null) {
                return (T) runEncoder((a,b) -> this.codec.sString(""), host, hints);
            }
            return this.codec.sString(((Enum)host).name());
        } else if (hostClass.getComponentType() != null) { // ARRAY
            ListLike list = this.codec.ListLike_new();
            int length = Array.getLength(host);
            for (int i = 0; i < length; i++) {
                this.codec.ListLike_add(list, serialize(Array.get(host, i)));
            }
            return (T) list;
        }
        if (host == null) {
            throw new IllegalStateException("If you intend to serialize and deserialize null objects you MUST regiester an encoder and decoder for that type");
        }
        // TODO Maybe asm to do direct field access instead of reflection/MethodHandles
        final List<SerialContext> ctxs = Stream
            .of(host.getClass().getDeclaredFields())
            .filter(f -> this.shouldSerialize(f, hostClass))
            .map(field -> new SerialContext(field.getType(), field.getName(), NoEx.run(() -> this.getLookup().unreflectGetter(field)), field.getAnnotation(SerializeHint.class)))
            .toList();

        Encoder serial = (_host, _hints)-> {
            KeyedListLike base = this.codec.KeyedList_new();
            for (SerialContext ctx : ctxs) {
                this.codec.KeyedList_add(base, ctx.fieldName(), serialize(ctx.fieldType(), NoEx.run(() -> ctx.handle().invoke(_host)), ctx.hints()));
            }
            return base;
        };
        if (cacheGenerated) this.SERIALIZE.put(hostClass, serial);
        return (T) runEncoder(serial, host, hints);
    }

    public <T> T deserialize(Class<T> host, GenericBase element) {
        return this.deserialize(host, element, DEFAULT_HINTS);
    }

    private <T> Decoder getRecordDecoder(Class<T> hostClass) {
        final MethodHandle ctor = NoEx.run(()->this.getLookup().unreflectConstructor(hostClass.getConstructors()[0]));
        final RecordComponent[] rcs = hostClass.getRecordComponents();

        final Decoder deser = (o,_hints) -> {
            final Object[] ctor_args = new Object[rcs.length];
            for(int i = 0; i < rcs.length; i++) {
                ctor_args[i] = deserialize(rcs[i].getType(), this.codec.KeyedList_get((KeyedListLike) o, rcs[i].getName()), getOrDefault(rcs[i]));
            }
            return NoEx.run(()-> ctor.invokeWithArguments(ctor_args));
        };
        return deser;
    }

    @Internal
    public <T> T deserialize(Class<T> hostClass, GenericBase element, SerializeHint hints) {
        element = lambdaGuard(element);
        hints = lambdaGuard(hints);
        if (this.DESERIALIZE.containsKey(hostClass)) {
            return (T) runDecoder((Decoder)this.DESERIALIZE.get(hostClass), element, hints);
            // return (T) ((Decoder)this.DESERIALIZE.get(hostClass)).decode(element, hints);
        } else if (hostClass.isRecord()) { // RECORD
            final Decoder deser = getRecordDecoder(hostClass);
            if (cacheGenerated) this.DESERIALIZE.put(hostClass, deser);
            return (T) runDecoder(deser, element, hints);
            // return (T) deser.decode(element, hints);
        } else if (hostClass.isEnum()) { // ENUM
            String s = this.codec.dsString(element);
            if (s.isEmpty()) return (T) runDecoder((a,b) -> null, element, hints);
            return (T) runDecoder((a,b)->Enum.valueOf((Class)hostClass, s), element, hints);
        } else if(hostClass.getComponentType() != null) { // ARRAY
            ListLike list = (ListLike) element;
            if (this.codec.ListLike_size(list) == 0) return (T) Array.newInstance(hostClass.getComponentType(), 0);
            Object newArray = Array.newInstance(hostClass.getComponentType(), this.codec.ListLike_size(list));
            for (int i = 0; i < this.codec.ListLike_size(list); i++) {
                Array.set(newArray, i, deserialize(hostClass.getComponentType(), this.codec.ListLike_get(list, i)));
            }
            return (T) newArray;
        }

        final List<SerialContext> ctxs = Stream
            .of(hostClass.getDeclaredFields())
            .filter(f -> shouldSerialize(f, hostClass))
            .map(field -> new SerialContext(field.getType(), field.getName(), NoEx.run(() -> getLookup().unreflectSetter(field)), field.getAnnotation(SerializeHint.class) == null ? spoofAnnotation(null, null, null) : field.getAnnotation(SerializeHint.class)))
            .toList();
        MethodHandle ctor = NoEx.run(() -> getLookup().unreflectConstructor(((Class)hostClass).getConstructor()));
        Decoder deser = (ele,_hints) -> {
            T obj = NoEx.runFunc(() -> (T) ctor.asType(ctor.type().changeReturnType(hostClass)).invoke()); //NoEx.run(() -> (T)getLookup().unreflectConstructor(hostClass.getConstructor()).invokeExact());
            KeyedListLike c = (KeyedListLike) ele;
            for (SerialContext ctx : ctxs) {
                if(!this.codec.KeyedList_contains(c, ctx.fieldName())) {
                    LOGGER.error("Class: %s has new field %s. Setting null!", hostClass.getCanonicalName(), ctx.fieldName());
                    NoEx.run(() -> ctx.handle().invoke(obj,null));
                    continue;
                }
                
                NoEx.run(()-> ctx.handle().invoke(obj, deserialize(ctx.fieldType(), this.codec.KeyedList_get(c, ctx.fieldName()), ctx.hints())));
            }
            return obj;
        };
        if (cacheGenerated) this.DESERIALIZE.put(hostClass, deser);
        return (T) runDecoder(deser, element, hints);
        // return (T) deser.decode(element, hints);
    }



    protected Lookup getLookup() {
        return MethodHandles.lookup();
    }

    protected boolean shouldSerialize(Field f, Class<?> hostClass) {
        if (!isPublic(f)) return false;
        if (Modifier.isFinal(f.getModifiers())) return false;
        if (isStatic(f)) return false;

        if (hostClass.getAnnotation(SerializeHint.class) != null && f.getAnnotation(SkipAutoSerialize.class) == null) {
            return true;
        } else if (f.getAnnotation(SerializeHint.class) != null) {
            return true;
        }
        return false;
    }

    protected KeyedListLike serializeListNoLookup(List o, SerializeHint hints) {
        ListLike list = this.codec.ListLike_new();

        Encoder serial = hints.collectionHint() == Void.class ?  null : this.SERIALIZE.get(hints.collectionHint());

        for (Object object : o) {
            if (serial != null) {
                
                codec.ListLike_add(list, (GenericBase) runEncoder(serial, o, DEFAULT_HINTS));

                // codec.ListLike_add(list, (GenericBase) serial.encode(o, DEFAULT_HINTS));
                // list.add(serial.serial(o, DEFAULT_HINTS));
            } else {
                // list.add(serialize(object));
                codec.ListLike_add(list, serialize(object));
            }
        }
        KeyedListLike keyedList = this.codec.KeyedList_new();
        if (serial == null) {
            // holder.putString("TYPE", o.get(0).getClass().getCanonicalName());
            this.codec.KeyedList_add(keyedList, "TYPE", this.codec.sString(o.get(0).getClass().getCanonicalName()));
        } else {
            // holder.putString("TYPE", hints.collectionHint().getCanonicalName());
            this.codec.KeyedList_add(keyedList, "TYPE", this.codec.sString(o.get(0).getClass().getCanonicalName()));
        }
        // holder.put("LIST", list);
        this.codec.KeyedList_add(keyedList, "LIST", list);
        return keyedList;
    }

    protected List deserializeListNolookup(KeyedListLike keyedList, SerializeHint hints) {
        Class<?> clazz = NoEx.run(() -> Class.forName(this.codec.KeyedList_get(keyedList, "TYPE").toString()));
        ListLike listLike = this.codec.KeyedList_get(keyedList, "LIST");

        List newList = Lists.newArrayListWithExpectedSize(this.codec.ListLike_size(listLike));
        int size = this.codec.ListLike_size(listLike);
        for (int i = 0; i < size; i++) {
            newList.add(deserialize(clazz, this.codec.ListLike_get(listLike, i)));
        }

        return newList;
    }

    public void defaultInit() {
        this.register(List.class, (list,hints) -> {
            if (list.isEmpty()) {
                return this.codec.KeyedList_new();
            } else if(!hints.collectionUseLookupTable()) {
                return serializeListNoLookup(list, hints);
            } else {
                // dedup the list
                List lookup = Lists.newArrayList(Sets.newHashSet(list));
                int[] collection = new int[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    collection[i] = lookup.indexOf(list.get(i));
                }
                KeyedListLike keyedList = this.codec.KeyedList_new();
                this.codec.KeyedList_add(keyedList, "LOOKUP", serialize(lookup));
                this.codec.KeyedList_add(keyedList, "COLLECTION", serialize(collection));
                // nbt.put("LOOKUP", serialize(lookup));
                // nbt.put("COLLECTION", serialize(collection));
                return keyedList;
            }
        }, (keyedList, hints) -> {
            if (this.codec.KeyedList_size((KeyedListLike)keyedList) == 0) return Collections.emptyList();
            else if(!hints.collectionUseLookupTable()) {
                return deserializeListNolookup(keyedList, hints);
            } else {
                List lookup = deserialize(List.class, this.codec.KeyedList_get(keyedList, "LOOKUP"));
                int[] collection = deserialize(int[].class, this.codec.KeyedList_get(keyedList, "COLLECTION"));
                List list = Lists.newArrayListWithExpectedSize(collection.length);
                for (int i : collection) {
                    list.add(lookup.get(i));
                }
                return list;
            }
        });
        this.register(Set.class, (set,hints) -> {
            if (set.isEmpty()) return this.codec.KeyedList_new();
            ListLike list = this.codec.ListLike_new();

            Encoder serial = hints.collectionHint() == Void.class ?  null : SERIALIZE.get(hints.collectionHint());

            for (Object object : set) {
                if (serial != null) {
                    this.codec.ListLike_add(list, (GenericBase) runEncoder(serial, object, DEFAULT_HINTS));
                    // this.codec.ListLike_add(list, (GenericBase) serial.encode(o, DEFAULT_HINTS));
                    // list.add(serial.serial(o, DEFAULT_HINTS));
                } else {
                    this.codec.ListLike_add(list, serialize(object));
                    // list.add(serialize(object));
                }
            }
            KeyedListLike holder = this.codec.KeyedList_new();
            if (serial == null) {
                this.codec.KeyedList_add(holder, "TYPE", this.codec.sString(set.iterator().next().getClass().getCanonicalName()));
                // holder.putString("TYPE", o.iterator().next().getClass().getCanonicalName());
            } else {
                this.codec.KeyedList_add(holder, "TYPE", this.codec.sString(hints.collectionHint().getCanonicalName()));

                // holder.putString("TYPE", hints.collectionHint().getCanonicalName());
            }
            this.codec.KeyedList_add(holder, "SET", list);
            // holder.put("SET", list);
            return holder;
        }, (nbtcompound,hints) -> {
            if (this.codec.KeyedList_size(nbtcompound) == 0) return Collections.emptySet();

            Class<?> clazz = NoEx.run(() -> Class.forName(this.codec.dsString(this.codec.KeyedList_get(nbtcompound, "TYPE"))));
            
            ListLike nbtlist = (ListLike) this.codec.KeyedList_get(nbtcompound, "SET");

            Set newList = Sets.newHashSetWithExpectedSize(this.codec.ListLike_size(nbtlist));
            for (int i = 0; i < this.codec.ListLike_size(nbtlist); i++) {
                // this.codec.addToListLike(nbtlist, obj);
                newList.add(deserialize(clazz, this.codec.ListLike_get(nbtlist, i)));
            }
            // nbtlist.forEach(ele -> newList.add(deserialize(clazz, ele)));
            return newList;
        });
        this.register(Map.class, (map,hints) -> {
            if (map.isEmpty()) return this.codec.ListLike_new();

            ListLike host = this.codec.ListLike_new();
            GenericBase keys;
            if (hints.mapKeyHint() != Void.class) {
                keys = serialize(map.keySet(), spoofAnnotation(null, null, hints.mapKeyHint()));
            } else {
                keys = serialize(map.keySet());
            }
            GenericBase vals;
            if (hints.mapValHint() != Void.class) {
                vals = serialize(Lists.newArrayList(map.values()), spoofAnnotation(null, null, hints.mapValHint()));
            } else {
                vals = serialize(Lists.newArrayList(map.values()));
            }
            this.codec.ListLike_add(host, keys);
            // host.add(keys);
            this.codec.ListLike_add(host, vals);
            // host.add(vals);
            return host;
        }, (nbtlist,hints) -> {
            
            if (this.codec.ListLike_size(nbtlist) == 0) return Collections.emptyMap();
        
            Map map = Maps.newHashMap();
            Iterator keys = deserialize(Set.class, this.codec.ListLike_get(nbtlist, 0)).iterator();
            Iterator vals = deserialize(List.class, this.codec.ListLike_get(nbtlist, 1)).iterator();
            while(keys.hasNext()) {
                map.put(keys.next(), vals.next());
            }
            return map;
        });
        this.register(UUID.class,    (uuid,hints) -> this.codec.sString(uuid.toString()),  (e,hints) -> UUID.fromString(this.codec.dsString(e)));
        this.register(String.class,  (s,hints) -> this.codec.sString(s),                   (e,hints) -> this.codec.dsString(e));
        this.register(Boolean.class, (b,hints) -> this.codec.sBoolean(b),                  (e,hints) -> this.codec.dsBoolean(e));
        this.register(Double.class,  (s,hints) -> this.codec.sDouble(s),                   (e,hints) -> this.codec.dsDouble(e));
        this.register(Float.class,   (s,hints) -> this.codec.sFloat(s),                    (e,hints) -> this.codec.dsFloat(e));
        this.register(Byte.class,    (b,hints) -> this.codec.sByte(b),                     (e,hints) -> this.codec.dsByte(e));
        this.register(Integer.class, (s,hints) -> this.codec.sInt(s),                      (e,hints) -> this.codec.dsInt(e));
        this.register(Long.class,    (s,hints) -> this.codec.sLong(s),                     (e,hints) -> this.codec.dsLong(e));
        this.register(boolean.class, (b,hints) -> this.codec.sBoolean(b),                  (e,hints) -> this.codec.dsBoolean(e));
        this.register(double.class,  (s,hints) -> this.codec.sDouble(s),                   (e,hints) -> this.codec.dsDouble(e));
        this.register(float.class,   (s,hints) -> this.codec.sFloat(s),                    (e,hints) -> this.codec.dsFloat(e));
        this.register(byte.class,    (b,hints) -> this.codec.sByte(b),                     (e,hints) -> this.codec.dsByte(e));
        this.register(int.class,     (s,hints) -> this.codec.sInt(s),                      (e,hints) -> this.codec.dsInt(e));
        this.register(long.class,    (s,hints) -> this.codec.sLong(s),                     (e,hints) -> this.codec.dsLong(e));
        this.register(Class.class, (clazz,hints) -> serialize(clazz.getCanonicalName()), (nbtString, hints) -> NoEx.run(()->Class.forName(deserialize(String.class, nbtString))));
    }

    @AvailableSince("0.2.6")
    public void defaultInitMinecraft() {
        // this.register(BlockState.class, 
        //     (bs,hints)-> BlockState.CODEC.stable().encodeStart(NbtOps.this, bs).getOrThrow(true, (s)->new RuntimeException(s)), 
        //     (BASE_TYPE,hints)->BlockState.CODEC.stable().decode(NbtOps.this, BASE_TYPE).getOrThrow(true, (str) -> new RuntimeException(str)).getFirst()
        // );
        this.register(Identifier.class, (id,hint)->this.serialize(String.class, id.toString()), (ele,hint) -> new Identifier(this.codec.dsString(ele)));
        this.register(Block.class, Registry.BLOCK);
        this.register(Item.class, Registry.ITEM);
        this.register(Enchantment.class, Registry.ENCHANTMENT);
        this.register(StatusEffect.class, Registry.STATUS_EFFECT);
        this.register(EntityType.class, (Registry)Registry.ENTITY_TYPE);
        this.register(BlockEntityType.class, (Registry)Registry.BLOCK_ENTITY_TYPE);
        // this.register(ItemStack.class, (stack,hints)-> stack.writeNbt(this.codec.getNewKeyedListLike()), (tag,hints)->ItemStack.fromNbt(tag));
    }
}
