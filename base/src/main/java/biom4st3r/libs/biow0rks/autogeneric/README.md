# AutoConverter
Through the power of generics you can automatically encode and decode a java object into any implemented type(nbt, json)

## Codec
### codec handles the specific format(json,nbt) operations like `add to json array`, `get NbtCompound size`, etc.

## @SerializeHint
### Annotation used to control more specific handling on fields
* When added to a class all(non-final, non-static) fields will attempt to be serialized
* May need to be added to fields for type handling
    * hinting at the type of a list, set, map, etc
    * has a `custom` field for you to register a serialization override
        * exmaple: serializing an int in to a hex string for user configuration
* `mapKeyHint` - the class that should be referenced for the key type
* `mapValHint` - the class that should be referenced for the value type
* `collectionsHint` - the class that should be referenced for the Collections type
* `collectionUseLookupTable` - enabled a compression method. Good for large lists with duplicate entries
* `custom` -  serialization override. Custom *coding implementation for that key. EX: `hex_string` for integers

## @SkipSerialization
* For when a class is annoated with @SerializeHint, but a specific field shouldn't be serialized
    * `boolean isDirty;` for example

## AutoConverter
* Special cases:
    * Records will be automatically *coded
    * Enums will be automatically *coded
    * Arrays will be automatically *coded
    * registered *coding statigics take president over the default.
* If a *coding statigy isn't registered when a type is requested the AutoConverter will make a best effort at generating one.
* All Best effort *coders are cached the same as a registered *coder
    * these can be overriden by registering over top them
* AutoConvert#serialize(Object) will work on all things except Collections
    * AutoConvert#serialize(Class<?>,Object) should be used for Lists, Setsm etc with the class as `List.class`, `Set.class`, etc.

## custom
* custom encoders can be reigster via AutoConvert#registerCustomAnnotationHandle
* the `key` is the same value as `custom`
* Custom*coders are ignored if they return null