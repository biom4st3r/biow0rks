package biom4st3r.libs.biow0rks.autogeneric;

import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.collect.Maps;

import biom4st3r.libs.biow0rks.autogeneric.AutoConvert.SerializeHint;

/**
 * Allow replacment of a standard encoder with a custom implementation on an annotation bases
 */
public final class HintHandler {

    public interface CustomEncoder {
        /**
         * Must return a type of the Codecs GenericBase.<p>
         * returning null causes the standard encoder to be used instead. For cases where you can't handled an object type.
         * @param codec
         * @param value
         * @return
         */
        @Nullable
        <T> T encode(Codec<T,? extends T,? extends T> codec, Object value);
    }

    public interface CustomDecoder {
        <O> Object decode(Codec<O,? extends O,? extends O> codec, O value);
    }

    public static record Pair(CustomEncoder encoder, CustomDecoder decoder) {}
    private static Pair EMPTY = new Pair(null, null);
    public final Map<String, Pair> HINTS = Maps.newHashMap();

    public CustomEncoder getEncoderHint(SerializeHint hint) {
        if (hint.custom().isBlank() ) return null;
        var i =  HINTS.getOrDefault(hint.custom(), EMPTY).encoder();
        if (i == null) throw new IllegalArgumentException("key: %s not found".replace("%s", hint.custom()));
        return i;
    }
    public CustomDecoder getDecoderHint(SerializeHint hint) {
        if (hint.custom().isBlank()) return null;
        var i = HINTS.getOrDefault(hint.custom(),EMPTY).decoder();
        if (i == null) throw new IllegalArgumentException("key: %s not found".replace("%s", hint.custom()));
        return i;
    }
    public void register(String s, CustomEncoder cEncoder, CustomDecoder cDecoder) {
        this.HINTS.put(s, new Pair(cEncoder, cDecoder));
    }
}
