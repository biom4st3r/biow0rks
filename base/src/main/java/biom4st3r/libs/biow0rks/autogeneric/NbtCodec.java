package biom4st3r.libs.biow0rks.autogeneric;

import net.minecraft.nbt.NbtByte;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtDouble;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtFloat;
import net.minecraft.nbt.NbtInt;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtLong;
import net.minecraft.nbt.NbtString;

@SuppressWarnings({"unchecked"})
public class NbtCodec implements Codec<NbtElement, NbtList, NbtCompound> {

    @Override
    public <T extends NbtElement> T sBoolean(boolean b) {
        return (T) NbtByte.of(b);
    }

    @Override
    public <T extends NbtElement> T sDouble(double b) {
        return (T) NbtDouble.of(b);
    }

    @Override
    public <T extends NbtElement> T sFloat(float b) {
        return (T) NbtFloat.of(b);
    }

    @Override
    public <T extends NbtElement> T sByte(byte b) {
        return (T) NbtByte.of(b);
    }

    @Override
    public <T extends NbtElement> T sInt(int b) {
        return (T) NbtInt.of(b);
    }

    @Override
    public <T extends NbtElement> T sLong(long b) {
        return (T) NbtLong.of(b);
    }

    @Override
    public <T extends NbtElement> T sString(String b) {
        return (T) NbtString.of(b);
    }

    @Override
    public <T extends NbtElement> boolean dsBoolean(T b) {
        return ((NbtByte)b).intValue() == 0 ? false : true;
    }

    @Override
    public <T extends NbtElement> double dsDouble(T b) {
        return ((NbtDouble)b).doubleValue();
    }

    @Override
    public <T extends NbtElement> float dsFloat(T b) {
        return ((NbtFloat)b).floatValue();
    }

    @Override
    public <T extends NbtElement> byte dsByte(T b) {
        return ((NbtByte)b).byteValue();
    }

    @Override
    public <T extends NbtElement> int dsInt(T b) {
        return ((NbtInt)b).intValue();
    }

    @Override
    public <T extends NbtElement> long dsLong(T b) {
        return ((NbtDouble)b).longValue();
    }

    @Override
    public <T extends NbtElement> String dsString(T b) {
        return ((NbtString)b).asString();
    }

    @Override
    public NbtList ListLike_new() {
        return new NbtList();
    }

    @Override
    public <T extends NbtElement> void ListLike_add(NbtList t, T obj) {
        t.add(obj);
    }

    @Override
    public int ListLike_size(NbtList like) {
        return like.size();
    }

    @Override
    public <T extends NbtElement> T ListLike_get(NbtList list, int i) {
        return (T) list.get(i);
    }

    @Override
    public NbtCompound KeyedList_new() {
        return new NbtCompound();
    }

    @Override
    public int KeyedList_size(NbtCompound keyed) {
        return keyed.getSize();
    }

    @Override
    public <T extends NbtElement> void KeyedList_add(NbtCompound t, String key, T obj) {
        t.put(key, obj);
    }

    @Override
    public <T extends NbtElement> T KeyedList_get(NbtCompound t, String key) {
        return (T) t.get(key);
    }

    @Override
    public boolean KeyedList_contains(NbtCompound keyed, String s) {
        return keyed.contains(s);
    }
    
}
