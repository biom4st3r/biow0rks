package biom4st3r.libs.biow0rks;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Supplier;

import net.minecraft.item.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.Internal;

/**
 * Item Component Object pool. Avoids creating objects when possible while providing and maintaining data within Items.<p>
 * When possible you should use `borrow*` instead of using `get`. <p>
 * Borrow automatically returns the objects while<p>
 * get must be manually return via {@link #release(boolean)}
 */
@AvailableSince("0.1.0")
public abstract class PooledItemComponent {

    public static final class ComponentKey<T extends PooledItemComponent> {

        @SuppressWarnings({"rawtypes"})
        private static final Map<Class<? extends PooledItemComponent>, ComponentKey> KEYS = Maps.newHashMap();

        @SuppressWarnings({"unchecked"})
        @Deprecated
        public static <T extends PooledItemComponent> ComponentKey<T> find(Class<T> clazz) {
            return KEYS.get(clazz);
        }
    
        private final void inspectSubType(Class<?> clazz, List<ComponentKey<?>> list) {
            if(clazz.isAssignableFrom(this.clazz)) list.add(this);
        }

        public enum Version {
            ZERO,
            ONE,
            ;
        }

        public final Class<T> clazz;
        public final String id;
        private final Supplier<T> newT;
        private final Queue<T> queue;
        private final ReentrantLock LOCK = new ReentrantLock();

        private ComponentKey(Class<T> clazz, String id, Supplier<T> newT) {
            this.clazz = clazz;
            this.id = id;
            this.newT = newT;
            this.queue = Queues.newArrayDeque();
            this.queue.add(newT.get());
        }

        @SuppressWarnings({"unchecked"})
        /**
         * You must use {@link ItemStack#getSubNbt(String)} with this `id` when serializing your components or override {@link #hasComponent(ItemStack)} with new component detection logic.
         * @param <T>
         * @param clazz
         * @param id
         * @param newT
         * @return
         */
        public static <T extends PooledItemComponent> ComponentKey<T> of(Class<T> clazz, String id, Supplier<T> newT) {
            ComponentKey<T> key = new ComponentKey<T>(clazz, id, newT);
            return KEYS.computeIfAbsent(clazz, c -> {
                for(Entry<Class<? extends PooledItemComponent>, List<ComponentKey<?>>> x : TRACKED_SUB_TYPES.entrySet()) {
                    key.inspectSubType(x.getKey(), x.getValue());
                }
                return key;
            });
        }

        private T getObject(ItemStack stack) {
            T rt = null;
            LOCK.lock();
            if(!queue.isEmpty()) {
                rt = queue.poll();
                LOCK.unlock();
            } else {
                LOCK.unlock();
                rt = newT.get();
            }
            rt.setStack(stack);
            return rt;
        }

        private Optional<T> fromStack(ItemStack stack) {
            T component = this.getObject(stack);
            if(!component.validItem(stack)) {
                component.release(false);
                return Optional.empty();
            }
            if(component.hasComponent(stack)) component.fromStack(stack);
            return Optional.of(component);
        }

        @SuppressWarnings({"unchecked"})
        private void returnComponent(PooledItemComponent component) {
            LOCK.lock();
            this.queue.add((T) component);
            LOCK.unlock();
        }

        public boolean borrowPooledComponent(ItemStack stack, Consumer<T> consumer, boolean serialize) {
            Optional<T> rt = this.fromStack(stack); // get
            rt.ifPresent(consumer); // enact
            PooledItemComponent.returnComponent(rt, serialize); // serialize, return
            return true;
        }

        public boolean borrowPooledComponents(Iterable<ItemStack> stacks, Consumer<Iterable<T>> consumer, boolean serialize) {
            List<T> list = Lists.newArrayList();
            for(ItemStack stack : stacks) this.fromStack(stack).ifPresent(list::add); // get
            
            consumer.accept(list); // enact

            PooledItemComponent.returnComponents(list, serialize); // serialize, return
            return true;
        }

        /**
         * Not actually deprecated, but this Component must be manually returned
         * @param <T>
         * @param clazz
         * @param stack
         * @return
         */
        @Deprecated
        public Optional<T> getComponent(ItemStack stack) {
            return this.fromStack(stack);
        }

        /**
         * Not actually deprecated, but this Component must be manually returned
         */
        @Deprecated
        public List<T> getComponents(List<ItemStack> stacks) {
            List<T> list = Lists.newArrayList();
            for(ItemStack stack : stacks) this.fromStack(stack).ifPresent(list::add);
            return list;
        }
        public final void noop() {}
    }

    @Internal
    private ItemStack stack;

    @Internal
    protected final ItemStack getStack() {
        return stack;
    }

    @Internal
    protected final void setStack(ItemStack stack) {
        this.stack = stack;
    }

    protected PooledItemComponent() {}
    
    private static final Map<
        Class<? extends PooledItemComponent>,
        List<ComponentKey<?>>
        > TRACKED_SUB_TYPES = Maps.newHashMap();

    /**
     * Not actually deprecated, but this component must be manually returned
     */
    @Deprecated
    public static <T extends PooledItemComponent> Iterable<T> getAllComponentOfSubType(Class<T> clazz, ItemStack stack) {
        List<T> list = Lists.newArrayList();
        for(ComponentKey<? extends T> x : getSubTypeOf(clazz)) x.getComponent(stack).ifPresent(list::add);
        return list;
    }

    public static <T extends PooledItemComponent>  void borrowAllComponentsOfSubType(Class<T> clazz, ItemStack stack, Consumer<Iterable<T>> consumer, boolean serialize) {
        Iterable<T> iterable = getAllComponentOfSubType(clazz, stack);
        consumer.accept(iterable);
        returnComponents(iterable, serialize);
    }

    /**
     * Returns all subtypes that are children of clazz type.
     * @param clazz
     * @return
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <T extends PooledItemComponent> List<ComponentKey<? extends T>> getSubTypeOf(Class<? extends T> clazz) {
        return (List) TRACKED_SUB_TYPES.getOrDefault(clazz, Collections.emptyList());
    }


    @SuppressWarnings({"unchecked"})
    public static <T extends PooledItemComponent> void registerSubType(Class<T> clazz) {
        TRACKED_SUB_TYPES.computeIfAbsent(clazz, c-> {
            List<ComponentKey<?>> list = Lists.newArrayList();
            ComponentKey.KEYS.values().forEach(k-> k.inspectSubType(clazz, list));
            return list;
        });
    }

    static {
        registerSubType(PooledItemComponent.class);
    }

    public static <T extends PooledItemComponent> void returnComponent(Optional<T> t, boolean serialize) {
        t.ifPresent(rt->rt.release(serialize));
    }

    public static <T extends PooledItemComponent> void returnComponents(Iterable<T> list, boolean serialize) {
        for(T t : list) {
            t.release(serialize);
        }
    }

    public static <T extends PooledItemComponent> void returnComponents(T[] array, boolean serialize) {
        for(T t : array) {
            t.release(serialize);
        }
    }

    /**
     * if `serialize` then {@link #toStack(ItemStack)} will be called if {@link #validItem(ItemStack)}
     * @param serialize
     */
    public final void release(boolean serialize) {
        if (serialize && this.validItem(this.getStack())) this.toStack(this.stack);
        this.clear();
        this.getKey().returnComponent(this);
    }

    /**
     * Handles component detection on ItemStack. This determines if fromStack gets called.
     * @param is
     * @return
     */
    @AvailableSince("0.2.2")
    public boolean hasComponent(ItemStack is) {
        return is.hasNbt() && is.getSubNbt(this.getKey().id) != null;
    }
    
    /**
     * Called if a host object is allocated and stack is {@link #validItem(ItemStack)}
     * @param stack
     */
    public abstract void fromStack(ItemStack stack);

    /**
     * Called immediately before the host object is release if it was returned with serialize true. <p>
     * Consider using an early return if the values in your object are default.<p>
     * NOTE: You MUST store data in the stack.getOrCreateSubTag(getKey().id) or override {@link #hasComponent(ItemStack)} with new component detection logic <p> 
     * @param stack
     */
    public abstract void toStack(ItemStack stack);

    public abstract void clear();

    public static void register(Class<? extends PooledItemComponent> clazz, Object newT) {
    }

    public abstract ComponentKey<? extends PooledItemComponent> getKey();

    public boolean validItem(ItemStack i) {
        return true;
    }
}
