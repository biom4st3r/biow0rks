package biom4st3r.libs.biow0rks;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

@AvailableSince("0.1.0")
public class NoEx {
    public interface NoExFunction<T> {
        T _run() throws Throwable;
        default T run() {
            try {
                return _run();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }
    public interface NoExRunnable {
        void _run() throws Throwable;
        default void run() {
            try {
                _run();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }
    public static <T> T run(NoExFunction<T> run) {
        return run.run();
    }
    public static <T> T runFunc(NoExFunction<T> run) {
        return run.run();
    }
    public static void run(NoExRunnable run) {
        run.run();
    }
    public static void runRun(NoExRunnable run) {
        run.run();
    }
}
