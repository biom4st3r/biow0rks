package biom4st3r.libs.biow0rks.rendering;


import java.util.Map;

import com.google.common.collect.Maps;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;

/**
 * RenderHelper
 */
@Environment(EnvType.CLIENT)
@AvailableSince("0.2.1")
public final class RenderHelper 
{
    private RenderHelper(){}

    public static int replaceByte(int color, int newBits,int shift)
    {
        color = ~(color & 0xFF << shift) & color;
        return (newBits << shift) | color;
    }

    public static int packLight(int skylevel, int blocklevel)
    {
        return (skylevel << 20) | (blocklevel<< 4);
    }

    @Deprecated
    public static void renderBox(float x, float y, float z, float width, float depth, float height,int scale,Sprite sprite,
    VertexConsumer vc, MatrixStack matrix, int light, int color)
    {
        yFace(x,-y+(scale-height),-z+(scale-depth), width, depth, true, sprite, vc, matrix, light, color, scale); // up
        xFace(x,y,z, width, height, true, sprite, vc, matrix, light,color,scale); // south
        xFace(-x+(scale-width),y,-z+(scale-depth), width, height, false, sprite, vc, matrix, light,color,scale); // north
        zFace(-x+(scale-width),y,-z+(scale-depth), depth, height, true, sprite, vc, matrix, light, color, scale); // east
        zFace(x,y,z, depth, height, false, sprite, vc, matrix, light, color, scale); // west
        yFace(-x+(scale-width),y,-z+(scale-depth), width, depth, false, sprite, vc, matrix, light, color, scale); // down
    }

    public static VertexConsumerProvider.Immediate createImmediate() {
        return VertexConsumerProvider.immediate(Util.make(() -> {
            Map<RenderLayer, BufferBuilder> builders = Maps.newLinkedHashMapWithExpectedSize(RenderLayer.getBlockLayers().size());
            for (RenderLayer layer : RenderLayer.getBlockLayers()) {
                builders.put(layer, new BufferBuilder(0xFFFF));
            }
            builders.put(RenderLayer.getDirectGlint(), new BufferBuilder(0xFFFF));
            builders.put(RenderLayer.getDirectEntityGlint(), new BufferBuilder(0xFFFF));
            builders.put(RenderLayer.getArmorGlint(), new BufferBuilder(0xFFFF));
            builders.put(RenderLayer.getArmorEntityGlint(), new BufferBuilder(0xFFFF));
            return builders;
        }), new BufferBuilder(0xFFFF));
    }

    public static BakedModel getModel(BlockState state)
    {
        return MinecraftClient.getInstance().getBakedModelManager().getBlockModels().getModel(state);
    }
    
    public static Sprite getBlockSprite(Identifier id)
    {
        return MinecraftClient.getInstance().getBakedModelManager().getAtlas(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE).getSprite(id);
    }

    public static void zFace(float x, float y, float z, float width, float height, boolean east, Sprite sprite,
    VertexConsumer vc, MatrixStack stack, int light, int color,float scale)
    {
        if(east)
        {
            x = scale-x;
        }
        else
        {
            width=-width;
            z = scale-z;
        }

        int[] normals = new int[]{
            1,1,1,
            1,1,1,
            1,1,1,
            1,1,1,};
        int ni = 0;
        float[] rgb = unpackRGB_F(color);
        vertex(vc, stack, x/scale, y/scale,          z/scale,         sprite.getMinU(), sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, x/scale, (y+height)/scale, z/scale,         sprite.getMinU(), sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, x/scale, (y+height)/scale, (z+width)/scale, sprite.getMaxU(), sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, x/scale, y/scale,          (z+width)/scale, sprite.getMaxU(), sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);

    }

    public static void yFace(float x, float y, float z, float width, float height, boolean top, Sprite sprite,
        VertexConsumer vc, MatrixStack stack, int light, int color,float scale)
    {
        if(top)
        {
            y = scale-y;
        }
        else
        {
            width=-width;
            x=scale-x;
        }
        int[] normals = new int[]{
            1,1,1,
            1,1,1,
            1,1,1,
            1,1,1,};
        int ni = 0;
        float[] rgb = unpackRGB_F(color);
        vertex(vc, stack, x/scale,         y/scale, z/scale,        sprite.getMinU(), sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, x/scale,         y/scale, (z+height)/scale, sprite.getMinU(), sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, (x+width)/scale, y/scale, (z+height)/scale, sprite.getMaxU(), sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);
        vertex(vc, stack, (x+width)/scale, y/scale, z/scale,          sprite.getMaxU(), sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);

    }

    public static void xFace(float x, float y, float z, float width, float height, boolean south, Sprite sprite,
        VertexConsumer vc, MatrixStack stack, int light,int color, float scale)
    {
        int[] normals = new int[]{
            1,1,1,
            1,1,1,
            1,1,1,
            1,1,1,};
        int ni = 0;
        if(south)
        {
            z = scale-z;
        }
        else
        {
            width=-width;
            x = scale-x;
        }
        float[] rgb = unpackRGB_F(color);
        vertex(vc, stack, x/scale,          y/scale,            z/scale, sprite.getMinU(),sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);// bottomleft
        vertex(vc, stack, (x+width)/scale,  y/scale,            z/scale, sprite.getMaxU(),sprite.getMinV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);//top left
        vertex(vc, stack, (x+width)/scale,  (y+height)/scale,   z/scale, sprite.getMaxU(),sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);// top right
        vertex(vc, stack, x/scale,          (y+height)/scale,   z/scale, sprite.getMinU(),sprite.getMaxV(),rgb[0],rgb[1],rgb[2],1,OverlayTexture.DEFAULT_UV, light, normals[ni++],normals[ni++],normals[ni++]);//bottom right
    }

    public static void vertex(VertexConsumer vc, MatrixStack stack,float x,float y,float z,float u, float v,float r,float g,float b,float a,int overlayuv,int lightuv,float nx,float ny,float nz)
    {
        vc.vertex(stack.peek().getPositionMatrix(),x,y,z)
            .color(r,g,b,a)
            .texture(u, v)
            .overlay(OverlayTexture.DEFAULT_UV)
            .light(lightuv)
            .normal(stack.peek().getNormalMatrix(),nx,ny,nz)
            .next();
    }

    public static int[] unpackRGB_I(int i)
    {
        return new int[]{(i >> 0 & 0xFF),(i >> 8 & 0xFF),(i >> 16 & 0xFF)};
    }
    
    public static float[] unpackRGB_F(int i)
    {
        return new float[]{(((i >> 16 & 0xFF))/256.0F),(((i >> 8 & 0xFF))/256.0F),(((i >> 0 & 0xFF))/256.0F)};
    }

    public static float[] unpackRGB_F(int i,float[] colors)
    {
        if(colors != null && colors.length >= 3)
        {
            colors[0] = (((i >> 16 & 0xFF))/256.0F);
            colors[1] = (((i >> 8 & 0xFF))/256.0F);
            colors[2] = (((i >> 0 & 0xFF))/256.0F);
            return null;
        }
        return new float[]{(((i >> 16 & 0xFF))/256.0F),(((i >> 8 & 0xFF))/256.0F),(((i >> 0 & 0xFF))/256.0F)};
    }

    public static float[] unpackRGBA_F(int i,float[] colors)
    {
        if(colors != null && colors.length >= 4)
        {
            colors[0] = (((i >> 16 & 0xFF))/256.0F);
            colors[1] = (((i >> 8 & 0xFF))/256.0F);
            colors[2] = (((i & 0xFF))/256.0F);
            colors[3] = (((i >> 24 & 0xFF))/256.0F);
            return colors;
        }
        return new float[]{(((i >> 16 & 0xFF))/256.0F),(((i >> 8 & 0xFF))/256.0F),(((i >> 0 & 0xFF))/256.0F)};
    }

    public static int packRGB_I(float red, float green, float blue) 
    {
		return 0xFF000000 | (int)(red*255) << 16 | (int)(green*255) << 8 | (int)(blue*255);
	}

    public static int packRGBA_I(int red,int green,int blue,int alpha)
    {
        return alpha << 24 | red << 16 | green << 8 | blue;
    }
    
}