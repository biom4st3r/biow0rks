package biom4st3r.libs.biow0rks.rendering;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import net.fabricmc.fabric.api.renderer.v1.mesh.Mesh;
import net.fabricmc.fabric.api.renderer.v1.mesh.MeshBuilder;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.util.math.BlockPos;

@AvailableSince("0.2.2")
public class MeshVertexConsumer implements VertexConsumer {
    MeshBuilder builder = RendererAccessHax.getRenderer().meshBuilder();
    QuadEmitter emitter = builder.getEmitter();
    int vertexIndex = 0;
    public BlockPos refPos = BlockPos.ORIGIN;

    @Override
    public VertexConsumer vertex(double x, double y, double z) {
        emitter.pos(vertexIndex, (float)x - refPos.getX(), (float)y - refPos.getY(), (float)z - refPos.getZ());
        return this;
    }

    @Override
    public VertexConsumer color(int red, int green, int blue, int alpha) {
        emitter.colorIndex(0);
        emitter.spriteColor(vertexIndex, 0, RenderHelper.packRGBA_I(red, green, blue, alpha));
        return this;
    }

    @Override
    public VertexConsumer texture(float u, float v) {
        emitter.sprite(vertexIndex, 0, u, v);
        return this;
    }

    @Override
    public VertexConsumer overlay(int u, int v) {
        return this;
    }

    @Override
    public VertexConsumer light(int u, int v) {
        emitter.lightmap(vertexIndex, RenderHelper.packLight(u, v));
        return this;
    }

    @Override
    public VertexConsumer normal(float x, float y, float z) {
        emitter.normal(vertexIndex, x, y, z);
        return this;
    }

    @Override
    public void next() {        
        vertexIndex++;
        if(vertexIndex > 3) {
            emitter.emit();
            vertexIndex = 0;
        }
    }

    @Override
    public void fixedColor(int red, int green, int blue, int alpha) {
    }

    @Override
    public void unfixColor() {
    }

    public Mesh build() {
        Mesh mesh = builder.build();
        emitter = builder.getEmitter();
        return mesh;
    }
    
}
