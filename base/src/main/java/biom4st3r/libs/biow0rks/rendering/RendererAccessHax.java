package biom4st3r.libs.biow0rks.rendering;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import net.fabricmc.fabric.api.renderer.v1.Renderer;
import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.impl.client.indigo.renderer.IndigoRenderer;

@AvailableSince("0.2.2")
public class RendererAccessHax {

	public static final Renderer getRenderer()
	{
		if(RendererAccess.INSTANCE.getRenderer() == null) return IndigoRenderer.INSTANCE;
		return RendererAccess.INSTANCE.getRenderer();
	}

}
