package biom4st3r.libs.biow0rks.rendering;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import com.mojang.datafixers.util.Pair;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.render.model.json.ModelOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;

@AvailableSince("0.2.1")
public abstract class AbstractAmbiguousModel implements BakedModel, FabricBakedModel, UnbakedModel {

    @Override
    public BakedModel bake(ModelLoader arg0, Function<SpriteIdentifier, Sprite> arg1, ModelBakeSettings arg2,
            Identifier arg3) {
        return this;
    }

    @Override
    public Collection<Identifier> getModelDependencies() {
        return Collections.emptyList();
    }

    @Override
    public Collection<SpriteIdentifier> getTextureDependencies(Function<Identifier, UnbakedModel> unbakedModelGetter,
            Set<Pair<String, String>> unresolvedTextureReferences) {
        return Collections.emptyList();
    }

    @Override
    public boolean isVanillaAdapter() {
        return false;
    }

    @Override
    public abstract void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos,
            Supplier<Random> randomSupplier, RenderContext context);

    @Override
    public abstract void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context);

    @Override
    public abstract ModelOverrideList getOverrides();

    @Override
    public abstract Sprite getParticleSprite();

    @Override
    public List<BakedQuad> getQuads(BlockState arg0, Direction arg1, Random arg2) {
        return Collections.emptyList();
    }

    @Override
    public abstract ModelTransformation getTransformation();

    @Override
    public boolean hasDepth() {
        return false;
    }

    @Override
    public boolean isBuiltin() {
        return false;
    }

    @Override
    public boolean isSideLit() {
        return false;
    }

    @Override
    public boolean useAmbientOcclusion() {
        return false;
    }
    
}
