package biom4st3r.libs.biow0rks;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import net.minecraft.nbt.NbtCompound;

/**
 * A Object for maintaining multiple resizable arrays of different types.<p>.
 * T represents an interaction medium for the types held in the Multilist.<p>
 * For implemented methods: any time you directly access the interal arrays you MUST either LOCK.lock() or this.borrow().
 * 
 */

@AvailableSince("0.2.1")
public abstract class MultiList<T> implements Iterable<T> {
    /**
     * for array copying without casting to Object[].
     * @param type
     * @param oldArray
     * @param length
     * @return
     */
    protected Object copyArray(Object oldArray, int length) {
        Object newArray = this.newArray(oldArray.getClass().getComponentType(), length);
        System.arraycopy(oldArray, 0, newArray, 0, Math.min(length, Array.getLength(oldArray)));
        return newArray;
    }

    public static final int DEFAULT_SIZE = 8;

    /**
     * UwU thwed safety.
     */
    private ReentrantLock LOCK = new ReentrantLock();
        
    public int loadFactor = DEFAULT_SIZE;

    protected Object[] collections;
    
    protected int trueSize = 0;

    protected final Class<?>[] types;

    public int getSignature() { // no clue if this is good.
        long l = this.trueSize;
        l = Long.rotateLeft(l, 15);
        for(Object o : this.collections) {
            l ^= o.hashCode();
            l = Long.rotateLeft(l, 15);
        }
        return (int) l;
    }

    /**
     * T's will be used. Do not store references to them
     * @return
     */
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int max = trueSize;
            int index = 0;
            T reusable = newT();
            @Override
            public boolean hasNext() {
                return index < max;
            }

            @Override
            public T next() {
                reassignT(index++, reusable);
                return reusable;
            }
            
        };
    }

    /**
     * Must be used to determine the real size of any internal array to avoid casting to Object[]
     */
    protected int internal_LengthOfArray(int i) {
        return Array.getLength(this.getCollection(i));
    }

    public MultiList(int defaultSize, Class<?>[] clazzes) {
        this.types = clazzes;
        this.reinit(defaultSize);
    }

    public MultiList(Class<?>[] clazzes) {
        this.types = clazzes;
        this.reinit(DEFAULT_SIZE);
    }
    protected MultiList(int defaultSize, int loadFactor, Class<?>[] clazzes) {
        this.types = clazzes;
        this.loadFactor = loadFactor;
        this.reinit(loadFactor);
    }

    /**
     * Will return an array of the types of collections this will hold.<p>
     * @return
     */
    protected Class<?>[] getTypes() {
        return types;
    }

    /**
     * must be borrowed during iteration
     * @param consumer
     */
    public void forEachEntry(Consumer<T> consumer) {
        for(int i = 0; i < this.length(); i++) {
            this.borrowEntry(i, consumer);
        }
    }

    // private static final MethodHandle OBJECT$CLONE = Util.make(()-> {
    //     try {
    //         return MethodHandles.lookup().unreflect(Object.class.getDeclaredMethod("clone"));
    //     } catch (IllegalAccessException | NoSuchMethodException | SecurityException e) {
    //         throw new RuntimeException(e);
    //     }
    // });

    @SuppressWarnings({"unchecked"})
    public <R extends MultiList<T>> R getCopy() {
        try {
            R r = (R) Stream.of(this.getClass().getConstructors()).filter(ctor->ctor.getParameterCount() == 0).findFirst().get().newInstance();
            r.collections = new Object[this.collections.length];
            for(int i = 0; i < this.getTypes().length; i++) { // TODO TEST
                Object old = r.collections[i];
                r.collections[i] = this.copyArray(this.collections[i], this.length());
                this.release(old);
            }
            r.trueSize = this.trueSize;
            r.clone = this.clone+1;
            return r;
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e) {
            throw new RuntimeException(e);
        }
    }

    public abstract NbtCompound toTag(NbtCompound tag);
    public abstract void fromTag(NbtCompound tag);

    /**
     * Returns a T with the values held at index i
     * @param <R>
     * @param i
     * @return
     */
    public T get(int i) {
        T t = this.newT();
        this.reassignT(i, t);
        return t;
    }


    private final T borrowable = this.newT();
    /**
     * Suppies a reused reassigned {@code T} to produce less waste than {@link MultiList.get}. Do not retain references.
     * @param t
     */
    public synchronized void borrowEntry(int i, Consumer<T> t) {
        this.reassignT(i, borrowable);
        t.accept(borrowable);
    }

    private final T mut = this.newT();
    /**
     * passes T to the consumer, then reassigns the indices to the altered values of T
     * @param index
     * @param entry
     */
    public synchronized void modifyEntry(int index, Consumer<T> entry) {
        this.reassignT(index, mut);
        entry.accept(mut);
        this.set(index, mut);
    }

    /**
     * Helper method to avoid havin to manually cast.
     * @param <R>
     * @param index
     * @return
     */
    @SuppressWarnings({"unchecked"})
    protected <R> R getCollection(int index) {
        return (R) this.collections[index];
    }

    public boolean isEmpty() {
        return this.length() == 0;
    }

    /**
     * removed null entries and shrinks contained arrays.
     * Check back to front until it finds a not null value and resizes down to that point
     */
    public void trim() {
        int size = this.internal_LengthOfArray(0);
        if(size > this.trueSize) {
            this.resize(this.trueSize - size);
        }
    }

    /**
     * Reinits arrays to default
     */
    public void clear() {
        this.reinit(this.loadFactor);
    }

    protected final void borrow(Runnable r) {
        this.LOCK.lock();
        r.run();
        this.LOCK.unlock();
    }

    /**
     * For impl caching. 
     * @param type
     * @param size
     * @return
     */
    protected Object newArray(Class<?> type, int size) {
        return Array.newInstance(type, size);
    }
    /**
     * Called when an array object will never be used again
     * @param obj
     */
    protected void release(Object obj) {
        if (obj == null) return;
        if (obj.getClass().componentType() == null) throw new IllegalArgumentException("Must be array");
    }

    /**
     * Reassigns contained arrays to {@code size} and resets index
     * 
     * @param size
     */
    public void reinit(int size) {
        this.borrow(()-> {
            collections = new Object[this.getTypes().length];
            int i = 0;
            for(Class<?> type : this.getTypes()) {
                Object old = collections[i];
                collections[i] = this.newArray(type, size);
                release(old);
                i++;
            }
            this.trueSize = 0;
        });
    }

    /**
     * For internal calling only. intended that you mutate collections inside of Runnable r<p>
     * Any MultiList.add(*) created, but either use this or call checkSize,borrow, and index++ like this.
     * @param r
     */
    protected final void _add(Runnable r) {
        this.checkSize();
        borrow(r);
        this.trueSize++;
    }

    public final void add(T t) {
        this.checkSize();
        this.set(this.length(), t);
        this.trueSize++;
    }

    /**
     * must lock this object
     * @param t
     */
    protected abstract void set(int index,T t);

    /**
     * checks if the array is at max capacity and expands if needed
     */
    protected final void checkSize() {
        if (this.trueSize == this.internal_LengthOfArray(0)) {
            this.resize(this.loadFactor);
        }
    }

    public final T getLatest() {
        return this.get(trueSize-1);
    }

    public void revokeLatest() {
        this.trueSize -= 1;
    }

    protected void resize(int expansion) {
        if(expansion == 0) return;
        int newSize = this.internal_LengthOfArray(0) + expansion;
        for(int i = 0; i < this.getTypes().length; i ++) {
            Object old = this.collections[i];
            this.collections[i] = this.copyArray(this.collections[i], newSize);
            this.release(old);
            // this.collections[i] = Arrays.copyOf(this.getCollection(i), newSize);
        }
    }

    int clone = 0;

    /**
     * returns true if anything was removed
     * @param pred
     * @return
     */
    public final boolean removeIf(Predicate<T> pred) {
        // TODO TEST
        IntList list = new IntArrayList(this.length() / 4);
        int[] index = {0};
        this.forEachEntry(t->{
            if(pred.test(t)) list.add(index[0]);
            index[0]++;
        });

        int start = -1;
        int end = -1;
        for(int i : list) { // Search for slices that can be removed.
            if(start == -1 && end == -1) {
                start = i;
                end = i;
                continue;
            }
            if(i == end+1) {
                end = i;
                continue;
            }
            if(start == end) { // To do as few array copies as possible
                this.remove(start);
            } else {
                this.removeRange(start, end+1);
            }
            start = -1;
            end = -1;
        }

        if(list.size() > 0) return true;
        return false;
    }

    /**
     * returns true if anything was removed
     * @param pred
     * @return
     */
    public final boolean retainIf(Predicate<T> pred) {
        // TODO TEST
        return this.removeIf(pred.negate());
    }

    public void removeRange(int start, int endExclusive) {
        // TODO TEST
        this.borrow(()-> {
            for(int i = 0; i < this.getTypes().length; i++) {
                Object array = this.getCollection(i);
                System.arraycopy(array, endExclusive, array, start, this.length()-endExclusive);
            }
        });
        trueSize -= (endExclusive-start); 
        this.trim();
    }

    protected abstract T newT();
    /**
     * Must borrow or lock while accessing internal arrays
     * @param i
     * @param t
     */
    protected abstract void reassignT(int i, T t);



    public void remove(int index) {
        this.borrow(()-> {
            for(int i = 0; i < this.getTypes().length; i++) {
                if(index == this.length()) {
                    this.revokeLatest();
                    continue;
                }
                System.arraycopy(this.getCollection(i),    index+1, this.getCollection(i),    index, this.trueSize-(index+1));
            }
        });
        this.revokeLatest();
        this.trim();
    }

    public int length() {
        return this.trueSize;
    }

}
