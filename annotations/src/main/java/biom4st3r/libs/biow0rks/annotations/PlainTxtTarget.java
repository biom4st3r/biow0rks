package biom4st3r.libs.biow0rks.annotations;

/**
 * An element annotated with this is targeted by name in plaintext by reflection, asm, or other systems 
 */
public @interface PlainTxtTarget { }
