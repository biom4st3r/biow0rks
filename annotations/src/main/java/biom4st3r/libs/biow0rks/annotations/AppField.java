package biom4st3r.libs.biow0rks.annotations;

/**
 * This element is only initlized on the AppClassLoader
 */
public @interface AppField { }
