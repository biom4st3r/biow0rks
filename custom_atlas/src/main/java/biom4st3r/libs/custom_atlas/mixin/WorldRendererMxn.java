package biom4st3r.libs.custom_atlas.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.render.Camera;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.render.LightmapTextureManager;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.WorldRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.Matrix4f;

@Mixin({WorldRenderer.class})
public abstract class WorldRendererMxn {

    @Shadow
    private void renderLayer(RenderLayer renderLayer, MatrixStack matrices, double d, double e, double f, Matrix4f positionMatrix) {
        
    }

    @Inject(
        method = "render", 
        at = @At(
            value = "INVOKE",
            shift = Shift.AFTER,
            target = "net/minecraft/client/render/RenderLayer.getCutout()Lnet/minecraft/client/render/RenderLayer;"
        )
    )
    private void renderCustomLayer0(MatrixStack matrices, float tickDelta, long limitTime, boolean renderBlockOutline, Camera camera, GameRenderer gameRenderer, LightmapTextureManager lightmapTextureManager, Matrix4f positionMatrix, CallbackInfo callbackInfo) {
        // this.renderLayer(TextureLoader.CARD_LAYER, matrices, camera.getPos().getX(), camera.getPos().getY(), camera.getPos().getZ(), positionMatrix);
    }
}
