package biom4st3r.libs.custom_atlas.mixin;

import java.util.Map;

import com.mojang.datafixers.util.Pair;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.SpriteAtlasManager;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.texture.TextureManager;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;

@Mixin({ModelLoader.class})
public class ModelLoaderMxn {
    @Shadow @Final
    private ResourceManager resourceManager;
    @Shadow
    private SpriteAtlasManager spriteAtlasManager;
    @Shadow @Final
    private Map<Identifier, Pair<SpriteAtlasTexture, SpriteAtlasTexture.Data>> spriteAtlasData;


    @Inject(
        method = "upload", 
        at = @At(
            value = "INVOKE",
            shift = Shift.BEFORE,
            target = "net/minecraft/client/render/model/SpriteAtlasManager.<init>(Ljava/util/Collection;)V"
        )
    )
    private void lateRegisterAtlas(TextureManager textureManager, Profiler profiler, CallbackInfoReturnable<SpriteAtlasManager> ci) {
        // TextureLoader.init(textureManager, resourceManager);
        // spriteAtlasData.put(TextureLoader.CARD_ATLAS_ID, Pair.of(TextureLoader.CARD_ATLAS, TextureLoader.DATA));
        // CARD_ATLAS = new SpriteAtlasTexture(CARD_ATLAS_ID);
        // DATA = CARD_ATLAS.stitch(manager, SPRITES.stream(), MinecraftClient.getInstance().getProfiler(), 4);

        // // Register the atlas
        // texturemanager.registerTexture(CARD_ATLAS.getId(), TextureLoader.CARD_ATLAS);
        // texturemanager.bindTexture(CARD_ATLAS.getId());
        // CARD_ATLAS.applyTextureFilter(DATA);
        // // Compile the texture data into atlas
        // // upload texture data to GPU
        // CARD_ATLAS.upload(DATA);
    }
}
