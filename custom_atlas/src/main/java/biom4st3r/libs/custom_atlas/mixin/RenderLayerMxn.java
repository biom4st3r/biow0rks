package biom4st3r.libs.custom_atlas.mixin;

import java.util.List;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.client.render.RenderLayer;

@Mixin({RenderLayer.class})
public class RenderLayerMxn {
    @Inject(at = @At("RETURN"), cancellable = true, method = "getBlockLayers")
    private static void smabs$customRenderLayer(CallbackInfoReturnable<List<RenderLayer>> ci) {
        // ci.setReturnValue(ImmutableList.<RenderLayer>builder().add(TextureLoader.CARD_LAYER).addAll(ci.getReturnValue()).build());
    }
}
