package biom4st3r.libs.custom_atlas.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.RenderLayers;
import net.minecraft.item.ItemStack;

@Mixin({RenderLayers.class})
public class RenderLayersMxn {
    @Inject(at = @At("RETURN"), method = "getItemLayer", cancellable = true)
    private static void forceItemRenderLayer(ItemStack stack, boolean direct, CallbackInfoReturnable<RenderLayer> ci) {
        // if (stack.getItem() instanceof CardItem) {
        //     ci.setReturnValue(TextureLoader.CARD_LAYER);
        // }
    }
}
