package biom4st3r.libs.custom_atlas;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexFormat;
import net.minecraft.client.render.VertexFormat.DrawMode;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.texture.SpriteAtlasTexture.Data;
import net.minecraft.util.Identifier;

public final class Survey {
    public static List<Survey> SURVEYS = Lists.newArrayList();

    private boolean renderBlocks;
    private boolean renderItems;
    private SpriteAtlasTexture atlas;
    private RenderLayer layer;
    private Data DATA;

    public void initAtlas() {
        // CARD_ATLAS = new SpriteAtlasTexture(CARD_ATLAS_ID);
        // DATA = CARD_ATLAS.stitch(manager, SPRITES.stream(), MinecraftClient.getInstance().getProfiler(), 4);

        // // Register the atlas
        // texturemanager.registerTexture(CARD_ATLAS.getId(), TextureLoader.CARD_ATLAS);
        // texturemanager.bindTexture(CARD_ATLAS.getId());
        // CARD_ATLAS.applyTextureFilter(DATA);
        // // Compile the texture data into atlas
        // // upload texture data to GPU
        // CARD_ATLAS.upload(DATA);
    }

    // public class Accessor {
    //     public final boolean _renderBlocks = renderBlocks;
    //     public final boolean _renderItems = renderItems;
    //     public final SpriteAtlasTexture _atlas = atlas;
    //     public final RenderLayer _layer = layer;
    // }

    public Survey shouldRenderBlocks(boolean bl) {
        this.renderBlocks = bl;
        return this;
    }

    public Survey shouldRenderItems(boolean bl) {
        this.renderItems = bl;
        return this;
    }

    public SpriteAtlasTexture getAtlas(Identifier id) {
        atlas =  new SpriteAtlasTexture(id);
        SURVEYS.add(this);
        return atlas;
    }

    public RenderLayer getLayer(String name, VertexFormat format, DrawMode mode, int expectedBufferSize, boolean translucent, RenderLayer.MultiPhaseParameters para) {
        this.layer = RenderLayer.of(name, format, mode, expectedBufferSize, renderBlocks, translucent, para);
        return this.layer;
    }

    public void offerLayer(RenderLayer layer) {

    }
}
