package biom4st3r.libs.bioecs.ecs.impl;

import java.util.Map;
import java.util.Set;

import biom4st3r.libs.bioecs.ecs.api.ComponentKey;
import biom4st3r.libs.bioecs.ecs.api.EntityDescription;

public class RegistryAccess {
    public static Map<EntityDescription, Set<ComponentKey<?>>> earlyAccess() {
        return ComponentRegistry.registry_map;
    }
}
