package biom4st3r.libs.bioecs.ecs.impl;

class MyOwn {
    interface BlackJack {}
    interface Hookers {}
    interface ECS {
        BlackJack getBlackjack();
        Hookers getHookers();
    }
}
