package biom4st3r.libs.bioecs.ecs.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.StreamSupport;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import biom4st3r.libs.bioecs.ecs.api.ComponentKey;
import biom4st3r.libs.bioecs.ecs.api.EntityDescription;
import biom4st3r.libs.bioecs.ecs.api.EntityDescriptionAssigner;
import biom4st3r.libs.biow0rks.BioLogger;

public class ComponentRegistry {
    static Map<EntityDescription, Set<ComponentKey<?>>> registry_map = Maps.newHashMap();
    static Map<String,ComponentKey<?>> keys = Maps.newHashMap();

    private static record Pair<T>(Iterable<T> iter, Predicate<T> pred, ComponentKey<?> key){}
    private static List<Pair<?>> sourceSets = Lists.newArrayList();

    public static <T> void register(Predicate<T> p, Iterable<T> sourceSet, ComponentKey<?> key) {
        sourceSets.add(new Pair<T>(sourceSet,p,key));
        StreamSupport.stream(sourceSet.spliterator(), false).filter(p).forEach(o->{
            Set<ComponentKey<?>> list = registry_map.computeIfAbsent((EntityDescription) o, desc->Sets.newHashSet());
            list.add(key);
            ((EntityDescriptionAssigner)o).add(key);
        });
    }

    public static <T> void register(T o, ComponentKey<?> key) {
        if(o instanceof EntityDescription ed) {
            Set<ComponentKey<?>> keys = registry_map.computeIfAbsent(ed, desc-> {
                Set<ComponentKey<?>> list = Sets.newHashSet();
                return list;
            });
            keys.add(key);
            ((EntityDescriptionAssigner)ed).add(key);
        } else {
            throw new RuntimeException();
        }
    }
    private static final BioLogger logger = new BioLogger("ComponentRegistry");
    public static void endRegistration() {
        
        catchLateRegistries();

        for(Entry<EntityDescription, Set<ComponentKey<?>>> entry : registry_map.entrySet()) {
            for(ComponentKey<?> x : entry.getValue()) {
                logger.log("Registering %s to %s", x.getIdentifier(), entry.getKey().toString());
            }
            ((EntityDescriptionAssigner)entry.getKey()).set(entry.getValue().toArray(ComponentKey<?>[]::new));
        }
        ComponentKeyImpl.LIST.forEach(key-> {
            keys.put(key.getIdentifier(), key);
        });
        registry_map.clear();
        registry_map = Collections.emptyMap();
    }

    @SuppressWarnings({"rawtypes","unchecked"})
    private static void catchLateRegistries() {
        for(Pair pair : sourceSets) {
            StreamSupport.stream(pair.iter().spliterator(), false).filter(pair.pred()).forEach(e->register(e,pair.key()));
        }
        sourceSets.clear();
        sourceSets = Collections.emptyList();
    }
}
