package biom4st3r.libs.bioecs.ecs.api;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

/**
 * Entity
 */
@AvailableSince("0.1.0")
public interface ComponentProvider {
    ComponentLookup getLookup();

    static ComponentProvider asComponentProvider(Object o) {
        return (ComponentProvider) o;
    }
}