package biom4st3r.libs.bioecs.ecs.api;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

/**
 * Doesn't use equals to compare. Simply asking if the component should sync
 */
@AvailableSince("0.1.4")
public interface SyncComponent extends Component {
    boolean shouldSync();
}
