package biom4st3r.libs.bioecs.ecs.api;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import net.minecraft.item.ItemStack;

@AvailableSince("0.1.0")
public interface ImplHelpers {
    static void copyInto(ItemStack oldStack, ItemStack newStack) {
        if(newStack == ItemStack.EMPTY || oldStack == ItemStack.EMPTY) {
            return;
        }
        ComponentProvider old = ComponentProvider.asComponentProvider(oldStack);
        ComponentProvider newp = ComponentProvider.asComponentProvider(newStack);
        old.getLookup().copyIntoNewLookup(newp.getLookup());
    }

    static boolean componentProviderCompare(Object o0, Object o1) {
        if(o0 instanceof ComponentLookup && o1 instanceof ComponentLookup) {
            return o0.equals(o1);
        }
        return ComponentProvider.asComponentProvider(o0).getLookup().equals(ComponentProvider.asComponentProvider(o1).getLookup());
    }
}
