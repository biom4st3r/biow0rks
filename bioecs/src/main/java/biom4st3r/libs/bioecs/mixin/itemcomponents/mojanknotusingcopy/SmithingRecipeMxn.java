package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.SmithingRecipe;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({SmithingRecipe.class})
public class SmithingRecipeMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/item/ItemStack.getNbt()Lnet/minecraft/nbt/NbtCompound;", 
            ordinal = 0, // Specificly near setTag
            shift = Shift.BEFORE), 
        method = "craft(Lnet/minecraft/inventory/Inventory;)Lnet/minecraft/item/ItemStack;", 
        cancellable = false, 
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$smithingrecipeOutputClone(Inventory inv, CallbackInfoReturnable<ItemStack> ci, ItemStack newStack) {
        ImplHelpers.copyInto(inv.getStack(0), newStack);
    }
}
