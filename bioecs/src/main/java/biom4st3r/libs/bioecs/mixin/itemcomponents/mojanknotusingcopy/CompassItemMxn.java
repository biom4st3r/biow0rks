package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.CompassItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({CompassItem.class})
public class CompassItemMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/item/ItemStack.setNbt(Lnet/minecraft/nbt/NbtCompound;)V", 
            ordinal = 0, 
            shift = Shift.AFTER), 
        method = "useOnBlock", 
        cancellable = false, 
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$compassCopyItem(ItemUsageContext context, CallbackInfoReturnable<ActionResult> ci, BlockPos pos, World world, PlayerEntity pe, ItemStack newStack) {
        ItemStack oldStack = context.getStack();
        ImplHelpers.copyInto(oldStack, newStack);
    }
}
