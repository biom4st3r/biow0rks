package biom4st3r.libs.bioecs.mixin.itemcomponents;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;

import biom4st3r.libs.bioecs.ecs.api.ComponentProvider;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({PacketByteBuf.class})
public abstract class MxnPacketByteBuff {

    @Shadow
    public abstract NbtCompound readNbt();
    @Shadow
    public abstract PacketByteBuf writeNbt(@Nullable NbtCompound compound);

    @Inject(
       at = @At(
          value="INVOKE",
          target="net/minecraft/network/PacketByteBuf.writeNbt(Lnet/minecraft/nbt/NbtCompound;)Lnet/minecraft/network/PacketByteBuf;",
          ordinal = -1,
          shift = Shift.AFTER),
       method = "writeItemStack",
       cancellable = false,
       locals = LocalCapture.NO_CAPTURE)
    private void bioecs$writeItemStack(ItemStack is, CallbackInfoReturnable<PacketByteBuf> ci) {
        ComponentProvider provider = ComponentProvider.asComponentProvider(is);
        if(provider.getLookup().size() > 0 || is.getItem().isNbtSynced()) {
            NbtCompound c = new NbtCompound();
            provider.getLookup().serialize(c);
            this.writeNbt(c);
        } else {
            this.writeNbt(null);
        }
    }

    @Inject(
       at = @At(value = "RETURN", ordinal = 1),
       method = "readItemStack",
       cancellable = false,
       locals = LocalCapture.NO_CAPTURE)
    private void bioecs$readItemStack(CallbackInfoReturnable<ItemStack> ci) {
        ItemStack is = ci.getReturnValue();
        ComponentProvider provider = ComponentProvider.asComponentProvider(is);
        NbtCompound compound = this.readNbt();
        if(!compound.isEmpty() && is.getItem().isNbtSynced()) {
            provider.getLookup().deserialize(compound);
        }
    }
}
