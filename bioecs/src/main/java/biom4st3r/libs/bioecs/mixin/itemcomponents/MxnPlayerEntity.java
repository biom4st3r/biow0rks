package biom4st3r.libs.bioecs.mixin.itemcomponents;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;

@Mixin({PlayerEntity.class})
public abstract class MxnPlayerEntity {
    @Shadow
    public abstract PlayerInventory getInventory();
}
