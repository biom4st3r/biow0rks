package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.server.filter.TextStream;
import net.minecraft.server.network.ServerPlayNetworkHandler;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({ServerPlayNetworkHandler.class})
public class ServerPlayNetworkHandlerMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/item/ItemStack.getNbt()Lnet/minecraft/nbt/NbtCompound;", 
            ordinal = 0, // Specificly near setTag
            shift = Shift.AFTER), 
        method = "addBook",
        cancellable = false, 
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$addBookClone(TextStream.Message title, List<TextStream.Message> pages, int slotId, CallbackInfo ci, ItemStack oldStack, ItemStack newStack) {
        ImplHelpers.copyInto(oldStack, newStack);
    }
}
