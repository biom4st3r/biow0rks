package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.DyeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.ShulkerBoxColoringRecipe;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({ShulkerBoxColoringRecipe.class})
public class ShulkerBoxColoringRecipeMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/item/ItemStack.hasNbt()Z", 
            ordinal = 0, // Specificly near setTag
            shift = Shift.BEFORE), 
        method = "craft(Lnet/minecraft/inventory/CraftingInventory;)Lnet/minecraft/item/ItemStack;", 
        cancellable = false, 
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$recolorShulkerClone(CraftingInventory craftingInventory, CallbackInfoReturnable<ItemStack> cir, ItemStack oldStack, DyeItem dyeItem, ItemStack newStack) {
        ImplHelpers.copyInto(oldStack, newStack);
    }
}
