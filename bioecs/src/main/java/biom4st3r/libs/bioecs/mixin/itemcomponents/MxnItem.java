package biom4st3r.libs.bioecs.mixin.itemcomponents;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;

import org.spongepowered.asm.mixin.Mixin;

import biom4st3r.libs.bioecs.ecs.api.ComponentKey;
import biom4st3r.libs.bioecs.ecs.api.EntityDescription;
import biom4st3r.libs.bioecs.ecs.api.EntityDescriptionAssigner;
import biom4st3r.libs.bioecs.ecs.impl.ComponentLookupImpl;
import biom4st3r.libs.bioecs.ecs.impl.RegistryAccess;
import net.minecraft.item.Item;

@Mixin({Item.class})
public class MxnItem implements EntityDescription, EntityDescriptionAssigner {
    private static final ComponentKey<?>[] EMPTY = new ComponentKey[0];
    ComponentKey<?>[] bioecs$keys = null;

    @Override
    public ComponentKey<?>[] getDescription() {
        if (bioecs$keys == null) {
            Set<ComponentKey<?>> set = RegistryAccess.earlyAccess().getOrDefault(this, Collections.emptySet());
            if (set.isEmpty()) return EMPTY;
            return set.toArray(ComponentKey[]::new);
        }
        return bioecs$keys;
    }

    @Override
    public void set(ComponentKey<?>[] keys) {
        List<ComponentKey<?>> list = Lists.newArrayList(keys);
        list.sort(ComponentLookupImpl.SORTER);
        bioecs$keys = list.toArray(ComponentKey[]::new);
    }

    @Override
    public void add(ComponentKey<?> key) {
    }
}
