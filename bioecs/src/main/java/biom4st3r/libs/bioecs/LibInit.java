package biom4st3r.libs.bioecs;

import biom4st3r.libs.bioecs.ecs.impl.ComponentRegistry;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public class LibInit implements ModInitializer {

	public static final String MODID = "bioecs";

	public static final Identifier SYNC_STACK = new Identifier(MODID, "sync");

	@Override
	public void onInitialize() {
		ServerLifecycleEvents.SERVER_STARTED.register((server)-> {
			ComponentRegistry.endRegistration();
		});
		if(FabricLoader.getInstance().isDevelopmentEnvironment()) {
			ComponentRegistry.register(Items.FISHING_ROD, SomeComponent.KEY);
		}
	}

	public static void sendSlotUpdate(ServerPlayerEntity player, int slot, ItemStack stack) {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		buf.writeInt(slot);
		buf.writeNbt(stack.writeNbt(new NbtCompound()));
		ServerPlayNetworking.send(player, SYNC_STACK, buf);
		// ServerPlayNetworking.createS2CPacket(SYNC_STACK, buf);
	}
}
