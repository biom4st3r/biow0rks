# Biow0rks
A library the used to be used by my mods to keep utitily. As I starting doing less modding keeping up this library to minecraft updates was to much overhead.

## Annotations
Annotation that I may want to use for documentation.


## Asm
Utilies that make working with JavaASM(objectweb asm) easier such as OpCodeMethodVisitor which lets you call opcodes directly instead of visit function. `.visitVarInsn(Opcodes.LSTORE, ...)` -> `.LSTORE(...)`


## Dimension
A tool I was working on to easily add Dimension dynamically(without restarting the client).


## Reflection
Classes for easily(and without catching) using MethodHandle reflection for constructors, fields, and methods


## Smile
Tools for using Instrumentation with the FabricClassLoader and for mass ASM.
